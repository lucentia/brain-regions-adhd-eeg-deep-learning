# A Novel Approach to Identify the Brain Regions that Best Classify ADHD by means of EEG and Deep Learning

## Name
Source code for the paper "A Novel Approach to Identify the Brain Regions that Best Classify ADHD by means of EEG and Deep Learning". 

## Description
In this project we provide source code to perform the experiments in our work.

## Installation

We strongly recommend using [TensorFlow Release 23.04](https://docs.nvidia.com/deeplearning/frameworks/tensorflow-release-notes/rel-23-04.html) to run the experiments, as it contains all the necessary Python packages with the appropriate version. We have executed the following command to create our Tensorflow docker, please change the names in <> according to your configuration:

```bash
docker run --gpus device=<YOUR_DEVICE> -it --rm --name <YOUR_DOCKER_NAME> -v <YOUR_LOCAL_VOLUME_PATH_01>:<YOUR_DOCKER_VOLUME_PATH_01> -v /<YOUR_DOCKER_VOLUME_PATH_02>:<YOUR_DOCKER_VOLUME_PATH_02> nvcr.io/nvidia/tensorflow:23.04-tf2-py3
```

For more information about Tensorflow and Docker and how to install it, see [Tensorflow Install Docker](https://www.tensorflow.org/install/docker).

## Usage

First clone the current repository, then make a copy "configuration_skel.json" and rename it to "configuration.json". Edit the "configuration.json" and write the absolute path to both your data and logs directories. Please note that the preprocessed data used in this paper can be found under de "data" directory.

```json
{
    "base_log_path":"ABSOLUTE_PATH_TO_LOGS",
    "base_data_path":"ABSOLUTE_PATH_TO_DATA"
}
```

Now just run the experiments into the Docker:

```bash
python launch_BrainRegions.py
python launch_BrainRegions_Forward.py
python launch_BrainRegions_Backward.py

```

## Support
For any support you can email the corresponding author Javier Sanchis <javier.sanchis@ua.es>

## Authors and acknowledgment
Javier Sanchis
Sandra García-Ponsoda
Miguel A. Teruel
Juan Trujillo
Il-Yeol Song

## License

## Project status
