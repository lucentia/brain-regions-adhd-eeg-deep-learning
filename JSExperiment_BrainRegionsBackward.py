# -*- coding: utf-8 -*-

import os
import random

import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold

from JSLib.JSLog import JSLog
from JSLib.JSData import JSData
from JSLib.JSModels import JSModel_MH_DSCNet
from JSLib.JSExperiment import JSExperiment

def JSCreate_Experiment(jsData, depth):

    experiment_name = "CH_{:02d}".format(depth)
    experiment_desc = "Experiment best channel combinations backward depth {}".format(depth)
    experiment_model = "MH_SCNet"


    jsExperiment = JSExperiment(
        experiment_desc, # Description
        experiment_name, # Name
        ['TIME', 'ITER', 'LOBE'], 
            # Parameters [
            #   ITER, 
            #   LOBE
            # ]
        experiment_model # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    sub_dir = "ch"
    jsLog = JSLog(jsExperiment.name)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)
    jsLog.write_output_log("EXPERIMENT NAME: {}".format(jsExperiment.name))

    return jsExperiment, jsLog

def JSExperiment_BrainRegionsForward():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '1'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    METRICS = [
        tf.keras.metrics.BinaryAccuracy(),
        tf.keras.metrics.Precision(),
        tf.keras.metrics.Recall(),
        tf.keras.metrics.TruePositives(),
        tf.keras.metrics.TrueNegatives(),
        tf.keras.metrics.FalsePositives(),
        tf.keras.metrics.FalseNegatives()
    ]

    best_lobe = None
    depth = 0

    while jsData.existsAvailableChannels():

        jsExperiment, jsLog = JSCreate_Experiment(jsData, depth)
        # Get Channels combinations
        
        
        jsData.setBackwardSFS(best_lobe)
        print(jsData.available_channels)
        jsLog.write_output_log("AVAILABLE CHANNELS: " + str(jsData.available_channels))

        print(jsData.channel_worst_combination)
        jsLog.write_output_log("WORST COMBINATION: " + str(jsData.channel_worst_combination))

        print(jsData.channel_combinations)
        jsLog.write_output_log("CHANNEL COMBINATIONS: " + str(jsData.channel_combinations))

        for param_iter in range(1,4):

            #Set Fold
            kf = KFold(n_splits=10)
            param_fold = 1

            for train, test in kf.split(index_subjects):
            
                train = train + 1
                test = test +1
                #print("%s %s" % (train, test))

                for param_lobe, locations in jsData.channel_combinations.items():
                    
                    jsLog.write_output_log("ITERATION: [{}] - FOLD: [{}]".format(
                        param_iter, param_fold))

                    start_time_fold = time.time()

                    partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical(train, test, 5, int_seed, param_lobe, "AUTO")

                    param_filters=64
                    param_epochs=15
                    param_activation='elu'
                    param_partial_output_classes=76
                    param_kernel_size = 128
                    param_dropout = 0.5
                    param_batch_size = 512
                    param_padding_mode='same'

                    model = JSModel_MH_DSCNet(
                        Chans = partial_x_train.shape[1], 
                        Samples = partial_x_train.shape[2], 
                        nb_classes=2, 
                        dropout_rate=param_dropout, 
                        filters = param_filters,
                        kernel_size=param_kernel_size, 
                        default_activation=param_activation,
                        partial_output_classes=param_partial_output_classes,
                        conv_1D_padding=param_padding_mode)

                    model.compile(
                        optimizer='adam',
                        loss='binary_crossentropy',
                        metrics=METRICS)

                    history = model.fit(
                        partial_x_train,
                        partial_y_train,
                        epochs=param_epochs,
                        batch_size=param_batch_size,
                        validation_data=(x_val, y_val),
                        verbose=1)

                    results = model.evaluate(x_test, y_test)

                    print(results)
                    jsLog.write_output_log("RESULTS [{}][{}] - [{}]".format(
                        param_iter, param_fold, results
                    ))
                    end_time_fold = time.time()

                    experiment_parameters_values = [
                        end_time_fold - start_time_fold,
                        param_iter, 
                        param_lobe
                        ]
                    

                    print(experiment_parameters_values)
                    jsLog.write_output_log("PARAMETERS VALUES: " + str(experiment_parameters_values))

                    print(jsData.channel_combinations[param_lobe])
                    jsLog.write_output_log("INDEX CHANNELS: " + str(jsData.channel_combinations[param_lobe]))


                    jsLog.JSSaveResults(
                        experiment_parameters_values, 
                        param_fold, 
                        results)

                    jsLog.JSSaveHistory(
                        history, 
                        experiment_parameters_values, 
                        jsExperiment.parameters, 
                        jsExperiment.get_history_filename(
                            experiment_parameters_values,
                            param_fold))
                
                    #jsLog.JSEvaluateBySubjectBinary(
                    #    model,
                    #    jsExperiment.parameters,
                    #    experiment_parameters_values,
                    #    test, 
                    #    jsData,
                    #    jsExperiment.get_predictions_filename(
                    #        experiment_parameters_values,
                    #        param_fold),
                    #    param_lobe)      

                param_fold += 1


        
        if jsData.existsAvailableChannels():
            best_new_channel, max_value_f1_score, max_index_f1_score = jsLog.JSGetBestChannel()
            depth += 1
            
            #best_new_channel = random.choice(jsData.available_channels)
            #best_lobe = random.choice(list(jsData.channel_combinations.keys()))
            best_lobe = max_index_f1_score
            print("BEST LOBE")
            print(depth)
            print(best_lobe)
            print("..................")
            jsLog.write_output_log("MAX VALUE: {} - BEST LOBE: {} - DEPTH: {}\n".format(
                max_value_f1_score,
                best_lobe, 
                depth))