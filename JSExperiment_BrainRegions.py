# -*- coding: utf-8 -*-

import random

import os
#import sys
#import collections
#import gc

import numpy as np
import tensorflow as tf

from sklearn.model_selection import KFold
#from sklearn.utils import shuffle

#from JSLog import JSLog
#from JSData import JSData
#from JSModels import JSModel_MH_DSCNet
#from JSExperiment import JSExperiment

from JSLib.JSLog import JSLog
from JSLib.JSData import JSData
from JSLib.JSModels import JSModel_MH_DSCNet
from JSLib.JSExperiment import JSExperiment

#from datetime import datetime
#from time import time

#from os import path

#from sklearn.metrics import confusion_matrix

def JSExperiment_BrainRegions():
    int_seed = 1234
    str_seed = '1234'

    os.environ['PYTHONHASHSEED'] = str_seed
    os.environ['TF_DETERMINISTIC_OPS'] = '1'

    np.random.seed(int_seed)
    random.seed(int_seed)
    tf.random.set_seed(int_seed)

    jsExperiment = JSExperiment(
        "MH_SCNet Hemispheres", # Description
        "MH_SCNet_FL_20_", # Name
        ['ITER', 'FOLD', 'LOBE'], 
            # Parameters [
            #   ITER, 
            #   FOLD,
            #   LOBE
            # ]
        'MH_SCNet' # model
        )

    print("EXPERIMENT NAME: ", jsExperiment.name)
    
    jsLog = JSLog(jsExperiment.name)

    data_type = 'raw'
    jsData = JSData(data_type)

    num_subjects = 60
    index_subjects = np.arange(1,num_subjects+1)

    jsExperiment.start_experiment()

    jsLog.init_results(jsExperiment.parameters)

    METRICS = [
        tf.keras.metrics.BinaryAccuracy(),
        tf.keras.metrics.Precision(),
        tf.keras.metrics.Recall(),
        tf.keras.metrics.TruePositives(),
        tf.keras.metrics.TrueNegatives(),
        tf.keras.metrics.FalsePositives(),
        tf.keras.metrics.FalseNegatives()
    ]

    for param_iter in range(1,4):

        #Set Fold
        kf = KFold(n_splits=10)
        param_fold = 1

        for train, test in kf.split(index_subjects):
        
            train = train + 1
            test = test +1
            print("%s %s" % (train, test))

            for param_lobe, locations in jsData.brain_locations.items():

                partial_x_train, partial_y_train, x_val, y_val, x_test, y_test = jsData.JSGetTrainTest_Categorical( train, test, 5, int_seed, param_lobe)

                print(param_lobe)

                print(partial_x_train.shape)
                print(partial_y_train.shape)

                print(x_val.shape)
                print(y_val.shape)

                print(x_test.shape)
                print(y_test.shape)

                print("Shape dims: ")
                print(partial_x_train.shape)
                print(jsData.np_data.shape)

                param_filters=64
                # ORIGINAL param_epochs=15
                param_epochs=20
                param_activation='elu'
                param_partial_output_classes=76
                param_kernel_size = 128
                param_dropout = 0.5
                param_batch_size = 128
                param_padding_mode='same'

                experiment_parameters_values = [
                    param_iter, 
                    param_lobe, 
                    param_fold
                    ]
                print(experiment_parameters_values)

                model = JSModel_MH_DSCNet(
                    Chans = partial_x_train.shape[1], 
                    Samples = partial_x_train.shape[2], 
                    nb_classes=2, 
                    dropout_rate=param_dropout, 
                    filters = param_filters,
                    kernel_size=param_kernel_size, 
                    default_activation=param_activation,
                    partial_output_classes=param_partial_output_classes,
                    conv_1D_padding=param_padding_mode)

                model.compile(
                    optimizer='adam',
                    loss='binary_crossentropy',
                    metrics=METRICS)

                print(model.summary())
                print(experiment_parameters_values)

                history = model.fit(
                    partial_x_train,
                    partial_y_train,
                    epochs=param_epochs,
                    batch_size=param_batch_size,
                    validation_data=(x_val, y_val),
                    verbose=1)

                results = model.evaluate(x_test, y_test)

                jsLog.JSSaveResults(
                    experiment_parameters_values, 
                    param_fold, 
                    results)

                #jsLog.JSSaveModel(
                #    model, 
                #    jsExperiment.get_model_filename(
                #        experiment_parameters_values,
                #        fold_iter))
                    
                jsLog.JSSaveHistory(
                    history, 
                    experiment_parameters_values, 
                    jsExperiment.parameters, 
                    jsExperiment.get_history_filename(
                        experiment_parameters_values,
                        param_fold))
                
                jsLog.JSEvaluateBySubjectBinary(
                    model,
                    jsExperiment.parameters,
                    experiment_parameters_values,
                    test, 
                    jsData,
                    jsExperiment.get_predictions_filename(
                        experiment_parameters_values,
                        param_fold),
                    param_lobe)        

            param_fold += 1