from time import time

class JSExperiment:

    def __init__(
            self, 
            experiment_description, 
            experiment_name,
            experiment_parameters,
            experiment_model):

        self.description = experiment_description
        self.name = experiment_name
        self.parameters = experiment_parameters
        self.model = experiment_model

        self.experiment_start = None
        self.experiment_end = None

    def start_experiment(self):
        self.experiment_start = time()

    def end_experiment(self):
        self.experiment_end = time()

    def experiment_time(self):

        if(self.experiment_start != None & self.experiment_end != None):
            self.experiment_end - self.experiment_start
        
        return None

    def get_fold_name(self, experiment_parameters_values, fold_iter):

        file_name = "{0}__FOLD_{1:02d}__".format(self.name, fold_iter)

        for p,d in zip(self.parameters, experiment_parameters_values):
            file_name = "{0}__{1}_{2}".format(file_name, p, d)

        return file_name.replace('.', '_')

    def get_model_filename(self, experiment_parameters_values, fold_iter):
        return self.model + "__" + self.get_fold_name(experiment_parameters_values, fold_iter) + ".h5"

    def get_history_filename(self, experiment_parameters_values, fold_iter):
        return self.model + "__" + self.get_fold_name(experiment_parameters_values, fold_iter) + "_history.csv"
    
    def get_predictions_filename(self, experiment_parameters_values, fold_iter):
        return self.model + "__" + self.get_fold_name(experiment_parameters_values, fold_iter) + "_predictions.csv"