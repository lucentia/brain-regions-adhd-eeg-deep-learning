"""
 ARL_EEGModels - A collection of Convolutional Neural Network models for EEG
 Signal Processing and Classification, using Keras and Tensorflow

 Requirements:
    (1) tensorflow == 2.X (as of this writing, 2.0 - 2.3 have been verified
        as working)
 
 To run the EEG/MEG ERP classification sample script, you will also need

    (4) mne >= 0.17.1
    (5) PyRiemann >= 0.2.5
    (6) scikit-learn >= 0.20.1
    (7) matplotlib >= 2.2.3
    
 To use:
    
    (1) Place this file in the PYTHONPATH variable in your IDE (i.e.: Spyder)
    (2) Import the model as
        
        from EEGModels import EEGNet    
        
        model = EEGNet(nb_classes = ..., Chans = ..., Samples = ...)
        
    (3) Then compile and fit the model
    
        model.compile(loss = ..., optimizer = ..., metrics = ...)
        fitted    = model.fit(...)
        predicted = model.predict(...)

 Portions of this project are works of the United States Government and are not
 subject to domestic copyright protection under 17 USC Sec. 105.  Those 
 portions are released world-wide under the terms of the Creative Commons Zero 
 1.0 (CC0) license.  
 
 Other portions of this project are subject to domestic copyright protection 
 under 17 USC Sec. 105.  Those portions are licensed under the Apache 2.0 
 license.  The complete text of the license governing this material is in 
 the file labeled LICENSE.TXT that is a part of this project's official 
 distribution. 
"""


from tensorflow import stack
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Concatenate, Average
from tensorflow.keras.layers import Dense, Activation, Permute, Dropout
from tensorflow.keras.layers import Conv1D, Conv2D, MaxPooling1D, MaxPooling2D, AveragePooling2D
from tensorflow.keras.layers import SeparableConv2D, DepthwiseConv2D, LSTM, SeparableConv1D
from tensorflow.keras.layers import BatchNormalization, AveragePooling1D
from tensorflow.keras.layers import SpatialDropout2D
from tensorflow.keras.regularizers import l1_l2
from tensorflow.keras.layers import Input, Flatten
from tensorflow.keras.constraints import max_norm
from tensorflow.keras.regularizers import l2
from tensorflow.keras import backend as K
from tensorflow.python.keras.backend import flatten
from tensorflow.python.keras.layers.merge import Average

from tensorflow.keras.models import Sequential

from tensorflow.keras.layers import (
    ConvLSTM2D,
    GlobalMaxPooling2D,
    GlobalAveragePooling2D,
    )
from tensorflow.python.ops.gen_array_ops import Transpose

def JSConvLSTM2D_seq(
    _filters = 128, 
    _kernel_size=(3,3), 
    _padding='same', 
    _data_format='channels_last',
    _return_sequences=False, 
    _input_shape=(15, 129, 56, 1),
    _nb_classes = 3):

    input_main = Input(shape=_input_shape)
    conv_layer = ConvLSTM2D(
        filters=64, 
        kernel_size=_kernel_size,
        input_shape=_input_shape,
        padding=_padding, 
        return_sequences=True)(input_main)
    #conv_layer = BatchNormalization()(conv_layer)
    conv_layer = ConvLSTM2D(
        filters=128, 
        kernel_size=_kernel_size,
        #input_shape=_input_shape,
        padding=_padding, 
        return_sequences=False)(conv_layer)
    #conv_layer = BatchNormalization()(conv_layer)
    #conv_layer = ConvLSTM2D(
    #    filters=256, 
    #    kernel_size=_kernel_size,
    #    input_shape=_input_shape,
    #    padding=_padding, 
    #    return_sequences=True)(conv_layer)
    #conv_layer = BatchNormalization()(conv_layer)
    #conv_layer = ConvLSTM2D(
    #    filters=32, 
    #    kernel_size=_kernel_size,
    #    input_shape=_input_shape,
    #    padding=_padding, 
    #    return_sequences=False)(conv_layer)

    #conv_layer = BatchNormalization()(conv_layer)
    conv_layer = GlobalAveragePooling2D()(conv_layer)

    conv_dense = Dense(128, activation='relu')(conv_layer)
    conv_dense  = Dropout(0.2)(conv_dense)
    conv_dense = Dense(64, activation='relu')(conv_dense)
    conv_dense  = Dropout(0.2)(conv_dense)
    conv_dense = Dense(32, activation='relu')(conv_dense)
    conv_dense  = Dropout(0.2)(conv_dense)

    conv_out = Dense(_nb_classes, activation='softmax')(conv_dense)

    return Model(inputs=input_main, outputs=conv_out)


def JSConvLSTM2d(
    _filters = 128, 
    _kernel_size=(3,3), 
    _padding='same', 
    _data_format='channels_last',
    _return_sequences=False, 
    _input_shape=(15, 129, 56, 1),
    _nb_classes = 3):
    
    input_main = Input(shape=_input_shape)
    conv_layer = ConvLSTM2D(
        filters=128,
        kernel_size=_kernel_size,
        padding=_padding,
        data_format=_data_format,
        return_sequences=True
        )(input_main)
    #conv_layer = GlobalMaxPooling2D()(conv_layer)
    conv_layer = ConvLSTM2D(
        filters=64,
        kernel_size=_kernel_size,
        padding=_padding,
        data_format=_data_format,
        return_sequences=True
        )(conv_layer)
    conv_layer = ConvLSTM2D(
        filters=32,
        kernel_size=_kernel_size,
        padding=_padding,
        data_format=_data_format,
        return_sequences=False
        )(conv_layer)
    conv_layer = GlobalMaxPooling2D()(conv_layer)

    conv_dense = Dense(64, activation='relu')(conv_layer)
    conv_dense  = Dropout(0.5)(conv_dense)
    conv_dense = Dense(32, activation='relu')(conv_dense)
    conv_dense  = Dropout(0.5)(conv_dense)
    
    conv_out = Dense(_nb_classes, activation='softmax')(conv_dense)
    return Model(inputs=input_main, outputs=conv_out)

def JSLSTM(timesteps, features, nb_classes=3, lstm_units=512):

    input_main = Input((features,timesteps))
    block1 = Permute((2, 1))(input_main)
    block1 = LSTM(lstm_units)(block1)
    block1 = Dropout(0.5)(block1)
    dense  = Dense(512, activation='elu')(block1)
    dense  = Dropout(0.5)(dense)
    dense  = Dense(128, activation='elu')(block1)
    dense  = Dropout(0.5)(dense)
    softmax = Dense(nb_classes, activation='softmax')(dense)
    return Model(inputs=input_main, outputs=softmax)

def JSSep(Chans = 56, Samples = 385, nb_classes=3, kernel_size=32):
    input_main = Input((Chans,Samples))
    
    #block1 = Permute((2, 1))(input_main)
    
    block1 = SeparableConv1D(kernel_size, 5, activation='relu')(input_main)
    block1 = AveragePooling1D(pool_size=(2))(block1)
    block1 = SeparableConv1D(kernel_size, 5, activation='relu')(block1)
    block1 = AveragePooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #lstm1 = LSTM(32, return_sequences=True)(block1)
    block2 = Flatten()(block1)
    block2 = Dropout(0.5)(block2)
    softmax = Dense(nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)
def JSCNN(Chans = 56, Samples = 385, nb_classes=3, kernel_size=32):
    input_main = Input((Chans,Samples))
    
    block1 = Permute((2, 1))(input_main)
    
    block1 = Conv1D(kernel_size, 32, activation='elu')(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    block1 = Conv1D(kernel_size, 32, activation='elu')(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    lstm1 = LSTM(4, return_sequences=True)(block1)
    block1 = BatchNormalization()(lstm1)
    block2 = Flatten()(block1)
    block2 = Dropout(0.5)(block2)
    softmax = Dense(nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)

def jsCNN1D(Chans = 56, Samples = 385, nb_classes=3, filters = 8, kernel_size=32, dropout_rate = 0.5):
    input_main = Input((Chans,Samples))
    
    block1 = Permute((2, 1))(input_main)
    
    block1 = Conv1D(filters, kernel_size, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size, 32, activation='elu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    block2 = Flatten()(block1)
    
    block2 = Dropout(dropout_rate)(block2)
    main_output = Dense(1, activation='sigmoid')(block2)

    return Model(inputs=input_main, outputs=main_output)

def jsCNN1D_M(Chans = 56, Samples = 385, nb_classes=3, filters = 8, kernel_size=32, dropout_rate = 0.5):
    input_main = Input((Chans,Samples))
    
    block1 = Permute((2, 1))(input_main)
    
    block1 = Conv1D(filters, kernel_size, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    block1 = Conv1D(filters, kernel_size, activation='relu')(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = Conv1D(kernel_size//2, 3, activation='relu')(block1)
    #block1 = MaxPooling1D(pool_size=(2))(block1)
    block2 = Flatten()(block1)
    
    block2 = Dropout(dropout_rate)(block2)
    main_output = Dense(1, activation='sigmoid')(block2)

    return Model(inputs=input_main, outputs=main_output)

def jsCNN1D_SepConv1D(Chans = 56, Samples = 385, nb_classes=3, filters = 8, kernel_size=32, dropout_rate = 0.5):
    input_main = Input((Chans,Samples))
    
    block1 = Permute((2, 1))(input_main)
    
    block1 = SeparableConv1D(filters, kernel_size, activation='relu')(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    block1 = SeparableConv1D(filters*2, kernel_size//2, activation='relu')(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    block1 = SeparableConv1D(filters*4, kernel_size//4, activation='relu')(block1)
    block2 = Flatten()(block1)
    
    block2 = Dropout(dropout_rate)(block2)
    main_output = Dense(1, activation='sigmoid')(block2)

    return Model(inputs=input_main, outputs=main_output)

def JSMultiBlock(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_multiplier=9):
    input_main = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input_main)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='ULF_Activation')(flatULF)
    normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(normalization_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='LF_Activation')(flatLF)
    normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(normalization_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='MF_Activation')(flatMD)
    normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(normalization_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='HF_Activation')(flatHF)
    normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(normalization_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UHF_Activation')(flatUHF)
    normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(normalization_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UUHF_Activation')(flatUUHF)
    normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(normalization_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(flatconcat)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input_main, outputs=modeloutput)

def JSModel_SepConv1D(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_multiplier=9):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='ULF_Activation')(flatULF)
    normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(normalization_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='LF_Activation')(flatLF)
    normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(normalization_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='MF_Activation')(flatMD)
    normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(normalization_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='HF_Activation')(flatHF)
    normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(normalization_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UHF_Activation')(flatUHF)
    normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(normalization_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UUHF_Activation')(flatUUHF)
    normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(normalization_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(flatconcat)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)

def JSModel_MH_DSCNet(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_classes=56, conv_1D_padding='valid'):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation='softmax', name='ULF_Activation')(flatULF)
    # normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation='softmax', name='LF_Activation')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation='softmax', name='MF_Activation')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation='softmax', name='HF_Activation')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation='softmax', name='UHF_Activation')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation='softmax', name='UUHF_Activation')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)


def JSModel_MultiHead_SepConv1D(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_classes=56, conv_1D_padding='valid'):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation='softmax', name='ULF_Activation')(flatULF)
    # normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation='softmax', name='LF_Activation')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation='softmax', name='MF_Activation')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation='softmax', name='HF_Activation')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation='softmax', name='UHF_Activation')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation='softmax', name='UUHF_Activation')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)

def JSModel_MultiHead_Activation_SepConv1D(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', default_activation_dense='relu', partial_output_classes=56, conv_1D_padding='valid'):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation=default_activation_dense, name='ULF_Activation')(flatULF)
    # normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation=default_activation_dense, name='LF_Activation')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation=default_activation_dense, name='MF_Activation')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation=default_activation_dense, name='HF_Activation')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation=default_activation_dense, name='UHF_Activation')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation=default_activation_dense, name='UUHF_Activation')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)

def JSModel_MultiHead_Activation_Normalization_SepConv1D(
    Chans = 56, 
    Samples = 385, 
    nb_classes=3, 
    dropout_rate=0.5, 
    filters_c1 = 16,
    filters_c2 = 16,
    kernel_size=64, 
    default_activation='relu', 
    default_activation_dense='relu', 
    partial_output_classes=56, 
    conv_1D_padding='valid'):

    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters_c1, kernel_size, activation=None, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = Activation(default_activation, name='ULF_Activation_01')(blockULF)
    blockULF = BatchNormalization(name='ULF_Normalization_01')(blockULF)
    blockULF = SeparableConv1D(filters_c2, kernel_size, activation=None, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = Activation(default_activation, name='ULF_Activation_02')(blockULF)
    blockULF = BatchNormalization(name='ULF_Normalization_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation=default_activation_dense, name='ULF_Dense')(flatULF)
    
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters_c1, kernel_size//2, activation=None, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = Activation(default_activation, name='LF_Activation_01')(blockLF)
    blockLF = BatchNormalization(name='LF_Normalization_01')(blockLF)
    blockLF = SeparableConv1D(filters_c2, kernel_size//2, activation=None, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = Activation(default_activation, name='LF_Activation_02')(blockLF)
    blockLF = BatchNormalization(name='LF_Normalization_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation=default_activation_dense, name='LF_Dense')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters_c1, kernel_size//4, activation=None, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = Activation(default_activation, name='MF_Activation_01')(blockMF)
    blockMF = BatchNormalization(name='MF_Normalization_01')(blockMF)
    blockMF = SeparableConv1D(filters_c2, kernel_size//4, activation=None, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = Activation(default_activation, name='MF_Activation_02')(blockMF)
    blockMF = BatchNormalization(name='MF_Normalization_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation=default_activation_dense, name='MF_Dense')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters_c1, kernel_size//8, activation=None, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = Activation(default_activation, name='HF_Activation_01')(blockHF)
    blockHF = BatchNormalization(name='HF_Normalization_01')(blockHF)
    blockHF = SeparableConv1D(filters_c2, kernel_size//8, activation=None, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = Activation(default_activation, name='HF_Activation_02')(blockHF)
    blockHF = BatchNormalization(name='HF_Normalization_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation=default_activation_dense, name='HF_Dense')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters_c1, kernel_size//16, activation=None, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = Activation(default_activation, name='UHF_Activation_01')(blockUHF)
    blockUHF = BatchNormalization(name='UHF_Normalization_01')(blockUHF)
    blockUHF = SeparableConv1D(filters_c2, kernel_size//16, activation=None, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = Activation(default_activation, name='UHF_Activation_02')(blockUHF)
    blockUHF = BatchNormalization(name='UHF_Normalization_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation=default_activation_dense, name='UHF_Dense')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters_c1, kernel_size//32, activation=None, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = Activation(default_activation, name='UUHF_Activation_01')(blockUUHF)
    blockUUHF = BatchNormalization(name='UUHF_Normalization_01')(blockUUHF)
    blockUUHF = SeparableConv1D(filters_c2, kernel_size//32, activation=None, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = Activation(default_activation, name='UUHF_Activation_02')(blockUUHF)
    blockUUHF = BatchNormalization(name='UUHF_Normalization_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation=default_activation_dense, name='UUHF_Dense')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dropout)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dropout)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)

def JSModel_MultiHead_Classification_SepConv1D(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, filters=16, kernel_size=64, default_activation='relu', partial_output_classes=56, conv_1D_padding='valid'):
    input = Input((Chans,Samples))
    
    input_permute = Permute((2, 1))(input)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    
    # ULF Block
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding, name='ULF_Conv_01')(input_permute)
    blockULF = SeparableConv1D(filters, kernel_size, activation=default_activation, padding=conv_1D_padding,name='ULF_Conv_02')(blockULF)
    blockULF = AveragePooling1D(pool_size=(2), name='ULF_Pool')(blockULF)
    #blockULF = LSTM(4, return_sequences=True)(blockULF)
    flatULF = Flatten(name='ULF_Flat')(blockULF)

    softmax_ULF = Dense(partial_output_classes, activation='softmax', name='ULF_Activation')(flatULF)
    # normalization_ULF = BatchNormalization(name='ULF_Normalization')(softmax_ULF)
    dropout_ULF = Dropout(dropout_rate, name='ULF_Dropout')(softmax_ULF)

    # LF Block
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_01')(input_permute)
    blockLF = SeparableConv1D(filters, kernel_size//2, activation=default_activation, padding=conv_1D_padding,name='LF_Conv_02')(blockLF)
    blockLF = AveragePooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    #blockLF = LSTM(4, return_sequences=True)(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(partial_output_classes, activation='softmax', name='LF_Activation')(flatLF)
    # normalization_LF = BatchNormalization(name='LF_Normalization')(softmax_LF)
    dropout_LF = Dropout(dropout_rate, name='LF_Dropout')(softmax_LF)

    # MF Block
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_01')(input_permute)
    blockMF = SeparableConv1D(filters, kernel_size//4, activation=default_activation, padding=conv_1D_padding,name='MF_Conv_02')(blockMF)
    blockMF = AveragePooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    #blockMF = LSTM(4, return_sequences=True)(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(partial_output_classes, activation='softmax', name='MF_Activation')(flatMD)
    # normalization_MF = BatchNormalization(name='MF_Normalization')(softmax_MF)
    dropout_MF = Dropout(dropout_rate, name='MF_Dropout')(softmax_MF)

    # HF Block
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_01')(input_permute)
    blockHF = SeparableConv1D(filters, kernel_size//8, activation=default_activation, padding=conv_1D_padding,name='HF_Conv_02')(blockHF)
    blockHF = AveragePooling1D(pool_size=(2), name='HF_Pool')(blockHF)
    #blockHF = LSTM(4, return_sequences=True)(blockHF)
    flatHF = Flatten(name='HF_Flat')(blockHF)

    softmax_HF = Dense(partial_output_classes, activation='softmax', name='HF_Activation')(flatHF)
    # normalization_HF = BatchNormalization(name='HF_Normalization')(softmax_HF)
    dropout_HF = Dropout(dropout_rate, name='HF_Dropout')(softmax_HF)

    # UHF Block
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_01')(input_permute)
    blockUHF = SeparableConv1D(filters, kernel_size//16, activation=default_activation, padding=conv_1D_padding,name='UHF_Conv_02')(blockUHF)
    blockUHF = AveragePooling1D(pool_size=(2), name='UHF_Pool')(blockUHF)
    #blockUHF = LSTM(4, return_sequences=True)(blockUHF)
    flatUHF = Flatten(name='UHF_Flat')(blockUHF)

    softmax_UHF = Dense(partial_output_classes, activation='softmax', name='UHF_Activation')(flatUHF)
    # normalization_UHF = BatchNormalization(name='UHF_Normalization')(softmax_UHF)
    dropout_UHF = Dropout(dropout_rate, name='UHF_Dropout')(softmax_UHF)

    # UUHF Block
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_01')(input_permute)
    blockUUHF = SeparableConv1D(filters, kernel_size//32, activation=default_activation, padding=conv_1D_padding,name='UUHF_Conv_02')(blockUUHF)
    blockUUHF = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(blockUUHF)
    #blockUUHF = LSTM(4, return_sequences=True)(blockUUHF)
    flatUUHF = Flatten(name='UUHF_Flat')(blockUUHF)

    softmax_UUHF = Dense(partial_output_classes, activation='softmax', name='UUHF_Activation')(flatUUHF)
    # normalization_UUHF = BatchNormalization(name='UUHF_Normalization')(softmax_UUHF)
    dropout_UUHF = Dropout(dropout_rate, name='UUHF_Dropout')(softmax_UUHF)

    concatBlock = Average(name='Concat_Blocks')([dropout_ULF, dropout_LF, dropout_MF, dropout_HF, dropout_UHF, dropout_UUHF])

    # flatconcat = Flatten(name='Flat_Concat')(concatBlock)
    dropout = Dropout(dropout_rate, name='Flat_Dropout')(concatBlock)
    
    classification = Dense(partial_output_classes*2, name='classification_01', activation=default_activation)(dropout)
    classification = Dropout(dropout_rate, name='classification_01_Dropout')(classification)
    
    classification = Dense((partial_output_classes*2)//2, name='classification_02', activation=default_activation)(classification)
    classification = Dropout(dropout_rate, name='classification_02_Dropout')(classification)
    
    classification = Dense((partial_output_classes*2)//4, name='classification_03', activation=default_activation)(classification)
    classification = Dropout(dropout_rate, name='classification_03_Dropout')(classification)

    if nb_classes == 2:
        classification = Dense(1, name = 'outputdense_bin')(classification)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(classification)

    else:
        classification = Dense(nb_classes, name = 'outputdense_multiclass')(classification)
        modeloutput  = Activation('softmax', name = 'softmax')(classification)

    # softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(dropout)

    return Model(inputs=input, outputs=modeloutput)

def JSMultiBlockWiseDrop(Chans = 56, Samples = 385, nb_classes=3, dropout_rate=0.5, ulf_size=64, num_kernels=16, partial_output_multiplier=9, conv_activation='elu'):
    input_main = Input((Chans,Samples))
    
    input_blocks = Permute((2, 1))(input_main)
    # normalization_input = BatchNormalization(name='Input_Normalization')(input_permute)
    # ULF Block
    ULF_conv_block = SeparableConv1D(num_kernels, ulf_size, name='ULF_conv_block_01')(input_blocks)
    ULF_conv_block = SeparableConv1D(num_kernels, ulf_size, name='ULF_conv_block_02')(ULF_conv_block)
    ULF_normalization = BatchNormalization(name='ULF_Normalization')(ULF_conv_block)
    ULF_activation = Activation(conv_activation, name='ULF_Activation')(ULF_normalization)
    ULF_polling = AveragePooling1D(pool_size=(2), name='ULF_Pool')(ULF_activation)
    ULF_flat = Flatten(name='ULF_Flat')(ULF_polling)

    ULF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='ULF_softmax')(ULF_flat)
    ULF_dropout = Dropout(dropout_rate, name='ULF_Dropout')(ULF_softmax)

    # LF Block
    LF_conv_block = SeparableConv1D(num_kernels, ulf_size//2, name='LF_conv_block_01')(input_blocks)
    LF_conv_block = SeparableConv1D(num_kernels, ulf_size//2, name='LF_conv_block_02')(LF_conv_block)
    LF_normalization = BatchNormalization(name='LF_Normalization')(LF_conv_block)
    LF_activation = Activation(conv_activation, name='LF_Activation')(LF_normalization)
    LF_polling = AveragePooling1D(pool_size=(2), name='LF_Pool')(LF_activation)
    LF_flat = Flatten(name='LF_Flat')(LF_polling)

    LF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='LF_softmax')(LF_flat)
    LF_dropout = Dropout(dropout_rate, name='LF_Dropout')(LF_softmax)

    # MF Block
    MF_conv_block = SeparableConv1D(num_kernels, ulf_size//4, name='MF_conv_block_01')(input_blocks)
    MF_conv_block = SeparableConv1D(num_kernels, ulf_size//4, name='MF_conv_block_02')(MF_conv_block)
    MF_normalization = BatchNormalization(name='MF_Normalization')(MF_conv_block)
    MF_activation = Activation(conv_activation, name='MF_Activation')(MF_normalization)
    MF_polling = AveragePooling1D(pool_size=(2), name='MF_Pool')(MF_activation)
    MF_flat = Flatten(name='MF_Flat')(MF_polling)

    MF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='MF_softmax')(MF_flat)
    MF_dropout = Dropout(dropout_rate, name='MF_Dropout')(MF_softmax)

    # HF Block
    HF_conv_block = SeparableConv1D(num_kernels, ulf_size//8, name='HF_conv_block_01')(input_blocks)
    HF_conv_block = SeparableConv1D(num_kernels, ulf_size//8, name='HF_conv_block_02')(HF_conv_block)
    HF_normalization = BatchNormalization(name='HF_Normalization')(HF_conv_block)
    HF_activation = Activation(conv_activation, name='HF_Activation')(HF_normalization)
    HF_polling = AveragePooling1D(pool_size=(2), name='HF_Pool')(HF_activation)
    HF_flat = Flatten(name='HF_Flat')(HF_polling)

    HF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='HF_softmax')(HF_flat)
    HF_dropout = Dropout(dropout_rate, name='HF_Dropout')(HF_softmax)

    # UHF Block
    UHF_conv_block = SeparableConv1D(num_kernels, ulf_size//16, name='UHF_conv_block_01')(input_blocks)
    UHF_conv_block = SeparableConv1D(num_kernels, ulf_size//16, name='UHF_conv_block_02')(UHF_conv_block)
    UHF_normalization = BatchNormalization(name='UHF_Normalization')(UHF_conv_block)
    UHF_activation = Activation(conv_activation, name='UHF_Activation')(UHF_normalization)
    UHF_polling = AveragePooling1D(pool_size=(2), name='UHF_Pool')(UHF_activation)
    UHF_flat = Flatten(name='UHF_Flat')(UHF_polling)

    UHF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UHF_softmax')(UHF_flat)
    UHF_dropout = Dropout(dropout_rate, name='UHF_Dropout')(UHF_softmax)

    # UUHF Block
    UUHF_conv_block = SeparableConv1D(num_kernels, ulf_size//32, name='UUHF_conv_block_01')(input_blocks)
    UUHF_conv_block = SeparableConv1D(num_kernels, ulf_size//32, name='UUHF_conv_block_02')(UUHF_conv_block)
    UUHF_normalization = BatchNormalization(name='UUHF_normalization')(UUHF_conv_block)
    UUHF_activation = Activation(conv_activation, name='UUHF_activation')(UUHF_normalization)
    UUHF_polling = AveragePooling1D(pool_size=(2), name='UUHF_Pool')(UUHF_activation)
    UUHF_flat = Flatten(name='UUHF_flat')(UUHF_polling)

    UUHF_softmax = Dense(nb_classes * partial_output_multiplier, activation='softmax', name='UUHF_softmax')(UUHF_flat)
    UUHF_dropout = Dropout(dropout_rate, name='UUHF_dropout')(UUHF_softmax)

    OUT_concatBlock = Average(name='Concat_Blocks')([ULF_dropout, LF_dropout, MF_dropout, HF_dropout, UHF_dropout, UUHF_dropout])

    OUT_flatconcat = Flatten(name='Flat_Concat')(OUT_concatBlock)
    OUT_dropout = Dropout(dropout_rate, name='Flat_Dropout')(OUT_flatconcat)

    softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(OUT_dropout)

    return Model(inputs=input_main, outputs=softmax_final)

def JSMultiBlockWise(Chans = 56, Samples = 385, nb_classes=3):
    input_permute = Input((Chans,Samples))
    
    # input_permute = Permute((2, 1))(input_main)
    
    # LF Block
    blockLF = SeparableConv1D(32, 32, activation='relu', name='LF_Input')(input_permute)
    blockLF = MaxPooling1D(pool_size=(2), name='LF_Pool')(blockLF)
    flatLF = Flatten(name='LF_Flat')(blockLF)

    softmax_LF = Dense(nb_classes * 3, activation='softmax', name='HF_Activation')(flatLF)

    # MF Block
    blockMF = SeparableConv1D(32, 16, activation='relu', name='MF_Input')(input_permute)
    blockMF = MaxPooling1D(pool_size=(2), name='MF_Pool')(blockMF)
    flatMD = Flatten(name='MF_Flat')(blockMF)

    softmax_MF = Dense(nb_classes * 3, activation='softmax', name='MF_Activation')(flatMD)

    # HD Block
    blockHD = SeparableConv1D(32, 8, activation='relu', name='HD_Input')(input_permute)
    blockHD = MaxPooling1D(pool_size=(2), name='HD_Pool')(blockHD)
    flatHD = Flatten(name='HD_Flat')(blockHD)

    softmax_HD = Dense(nb_classes * 3, activation='softmax', name='HD_Activation')(flatHD)

    concatBlock = Concatenate(name='Concat_Blocks')([softmax_LF, softmax_MF, softmax_HD])

    flatconcat = Flatten(name='Flat_Concat')(concatBlock)

    softmax_final = Dense(nb_classes, activation='softmax', name='Softmax_General_Output')(flatconcat)

    return Model(inputs=input_permute, outputs=softmax_final)


def JSCNN_02(Chans = 56, Samples = 385, nb_classes=3):
    input_main = Input((Chans,Samples))
    # block1 = Permute((2, 1))(input_main)
    block1 = Conv1D(128, 3, padding = 'same', activation='relu')(input_main)
    block1 = BatchNormalization()(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    block1 = Conv1D(128, 3, padding = 'same', activation='relu')(block1)
    block1 = BatchNormalization()(block1)
    block1 = MaxPooling1D(pool_size=(2))(block1)
    block2 = Flatten()(block1)
    block2 = Dense(64, activation='tanh')(block2)
    block2 = Dropout(0.2)(block2)
    block2 = Dense(32, activation='tanh')(block2)
    block2 = Dropout(0.2)(block2)
    block2 = Dense(16, activation='tanh')(block2)
    block2 = Dropout(0.2)(block2)
    softmax = Dense(nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)

def JSEEGCnn(Chans = 56, Samples = 385, F1 = 128, F2 = 4):
    input_main  = Input(shape = (Chans*Samples, ))
    dense       = Dense(F1, activation='relu', use_bias=True)(input_main)
    dense       = Dense(F2, activation='relu')(dense)
    softmax     = Dense(3, activation='softmax')(dense)

    return Model(inputs=input_main, outputs=softmax) 

def JSModel_MLP_03(chans=19, samples=250, U1 = 4, U2 = 16, U3 = 4, use_bias_in = False, dropout_block = 0.5, nb_classes=3, dense_activation='relu'):
    
    input_main  = Input((chans,samples))

    flatten     = Flatten()(input_main)

    dense       = Dense(U1, activation=dense_activation, use_bias = use_bias_in)(flatten)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U2, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U3, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dense)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dense)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    #softmax     = Dense(nb_classes, activation='softmax')(dense)
    #sigmoid     = Dense(3, activation='sigmoid')(dense)

    return Model(inputs=input_main, outputs=modeloutput) 

def JSModel_MLP_05(chans=19, samples=250, U1 = 4, U2 = 16, U3 = 4, U4 = 4, U5 = 4, use_bias_in = False, dropout_block = 0.5, nb_classes=3, dense_activation='relu'):
    
    input_main  = Input((chans,samples))

    flatten     = Flatten()(input_main)

    dense       = Dense(U1, activation=dense_activation, use_bias = use_bias_in)(flatten)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U2, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U3, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U4, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    dense       = Dense(U5, activation=dense_activation, use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block)(dense)

    if nb_classes == 2:
        dense     = Dense(1, name = 'outputdense_bin')(dense)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'outputdense_multiclass')(dense)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)

    #softmax     = Dense(nb_classes, activation='softmax')(dense)
    #sigmoid     = Dense(3, activation='sigmoid')(dense)

    return Model(inputs=input_main, outputs=modeloutput) 

def JSModel_MLP_04(Chans = 56, Samples = 385, F1 = 4, F2 = 32, F3 = 64, F4 = 16, use_bias_in = False, dropout_block1 = 0.5):
    input_main  = Input(shape = (Chans*Samples, ))
    dense       = Dense(F1, activation='relu', use_bias = use_bias_in)(input_main)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(F2, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F3, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F4, activation='relu', use_bias = use_bias_in)(dense)
    sigmoid     = Dense(1, activation='sigmoid')(dense)

    return Model(inputs=input_main, outputs=sigmoid) 

def JSModel_MLP_05_DR_02(Chans = 56, Samples = 385, F1 = 4, F2 = 32, F3 = 64, F4 = 16, F5 = 16, use_bias_in = False, dropout_block1 = 0.5):
    input_main  = Input(shape = (Chans*Samples, ))
    dense       = Dense(F1, activation='relu', use_bias = use_bias_in)(input_main)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(F2, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(F3, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F4, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F5, activation='relu', use_bias = use_bias_in)(dense)
    sigmoid     = Dense(1, activation='sigmoid')(dense)

    return Model(inputs=input_main, outputs=sigmoid) 

def JSModel_MLP_05_DR_03(Chans = 56, Samples = 385, F1 = 4, F2 = 32, F3 = 64, F4 = 16, F5 = 16, use_bias_in = False, dropout_block1 = 0.5):
    input_main  = Input(shape = (Chans*Samples, ))
    dense       = Dense(F1, activation='relu', use_bias = use_bias_in)(input_main)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(F2, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(F3, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F4, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dense(F5, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    sigmoid     = Dense(1, activation='sigmoid')(dense)

    return Model(inputs=input_main, outputs=sigmoid)

def jsModel_MLP_simple(
    vector_length, 
    nb_classes=3, 
    dropout_block1 = 0.5, 
    use_bias_in=True, 
    _activation='relu',
    _FM=128,
    _l2_reg_value = 0.001, 
    _kernel_regularizer = None):
    
    input_main  = Input(shape = (vector_length, ))
    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(input_main)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    dense       = Dense(_FM, activation=_activation, use_bias = use_bias_in, kernel_regularizer=_kernel_regularizer)(dense)
    dense       = Dropout(dropout_block1)(dense)

    softmax     = Dense(nb_classes, activation='softmax')(dense)

    return Model(inputs=input_main, outputs=softmax)


def jsModel_MLP_64(vector_length, nb_classes=3, dropout_block1 = 0.5, use_bias_in=True):
    input_main  = Input(shape = (vector_length, ))
    dense       = Dense(64, activation='relu', use_bias = use_bias_in)(input_main)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(128, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(256, activation='relu', use_bias = use_bias_in)(input_main)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(256, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(128, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    dense       = Dense(64, activation='relu', use_bias = use_bias_in)(dense)
    dense       = Dropout(dropout_block1)(dense)
    softmax     = Dense(nb_classes, activation='softmax')(dense)

    return Model(inputs=input_main, outputs=softmax)

def EEGNet(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, kernLength = 64, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout'):
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation('elu')(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation('elu')(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)
    
    if nb_classes == 2:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)

def jsEEGNet(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, kernLength = 64, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout', activation='elu'):
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation(activation)(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation(activation)(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)

    
    
    if nb_classes == 1:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)

def jsEEGNetDense(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, kernLength = 64, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout', activation='elu'):
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation(activation)(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation(activation)(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)

    block3       = Dense(128)(flatten)
    block3       = Activation(activation)(block3)
    block3       = Dense(64)(block3)
    block3       = Activation(activation)(block3)
    block3       = Dense(32)(block3)
    block3       = Activation(activation)(block3)
    
    if nb_classes == 1:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(block3)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(block3)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)




def EEGNet_SSVEP(nb_classes = 12, Chans = 8, Samples = 256, 
             dropoutRate = 0.5, kernLength = 256, F1 = 96, 
             D = 1, F2 = 96, dropoutType = 'Dropout'):
    """ SSVEP Variant of EEGNet, as used in [1]. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. 
      D               : number of spatial filters to learn within each temporal
                        convolution.
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.
      
      
    [1]. Waytowich, N. et. al. (2018). Compact Convolutional Neural Networks
    for Classification of Asynchronous Steady-State Visual Evoked Potentials.
    Journal of Neural Engineering vol. 15(6). 
    http://iopscience.iop.org/article/10.1088/1741-2552/aae5d8

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation('elu')(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation('elu')(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)
    
    dense        = Dense(nb_classes, name = 'dense')(flatten)
    softmax      = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=softmax)



def EEGNet_old(nb_classes, Chans = 64, Samples = 128, regRate = 0.0001,
           dropoutRate = 0.25, kernels = [(2, 32), (8, 4)], strides = (2, 4)):
    """ Keras Implementation of EEGNet_v1 (https://arxiv.org/abs/1611.08024v2)

    This model is the original EEGNet model proposed on arxiv
            https://arxiv.org/abs/1611.08024v2
    
    with a few modifications: we use striding instead of max-pooling as this 
    helped slightly in classification performance while also providing a 
    computational speed-up. 
    
    Note that we no longer recommend the use of this architecture, as the new
    version of EEGNet performs much better overall and has nicer properties.
    
    Inputs:
        
        nb_classes     : total number of final categories
        Chans, Samples : number of EEG channels and samples, respectively
        regRate        : regularization rate for L1 and L2 regularizations
        dropoutRate    : dropout fraction
        kernels        : the 2nd and 3rd layer kernel dimensions (default is 
                         the [2, 32] x [8, 4] configuration)
        strides        : the stride size (note that this replaces the max-pool
                         used in the original paper)
    
    """

    # start the model
    input_main   = Input((Chans, Samples))
    layer1       = Conv2D(16, (Chans, 1), input_shape=(Chans, Samples, 1),
                                 kernel_regularizer = l1_l2(l1=regRate, l2=regRate))(input_main)
    layer1       = BatchNormalization()(layer1)
    layer1       = Activation('elu')(layer1)
    layer1       = Dropout(dropoutRate)(layer1)
    
    permute_dims = 2, 1, 3
    permute1     = Permute(permute_dims)(layer1)
    
    layer2       = Conv2D(4, kernels[0], padding = 'same', 
                            kernel_regularizer=l1_l2(l1=0.0, l2=regRate),
                            strides = strides)(permute1)
    layer2       = BatchNormalization()(layer2)
    layer2       = Activation('elu')(layer2)
    layer2       = Dropout(dropoutRate)(layer2)
    
    layer3       = Conv2D(4, kernels[1], padding = 'same',
                            kernel_regularizer=l1_l2(l1=0.0, l2=regRate),
                            strides = strides)(layer2)
    layer3       = BatchNormalization()(layer3)
    layer3       = Activation('elu')(layer3)
    layer3       = Dropout(dropoutRate)(layer3)
    
    flatten      = Flatten(name = 'flatten')(layer3)
    
    dense        = Dense(nb_classes, name = 'dense')(flatten)
    softmax      = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input_main, outputs=softmax)



def DeepConvNet(nb_classes, Chans = 64, Samples = 256,
                dropoutRate = 0.5):
    """ Keras implementation of the Deep Convolutional Network as described in
    Schirrmeister et. al. (2017), Human Brain Mapping.
    
    This implementation assumes the input is a 2-second EEG signal sampled at 
    128Hz, as opposed to signals sampled at 250Hz as described in the original
    paper. We also perform temporal convolutions of length (1, 5) as opposed
    to (1, 10) due to this sampling rate difference. 
    
    Note that we use the max_norm constraint on all convolutional layers, as 
    well as the classification layer. We also change the defaults for the
    BatchNormalization layer. We used this based on a personal communication 
    with the original authors.
    
                      ours        original paper
    pool_size        1, 2        1, 3
    strides          1, 2        1, 3
    conv filters     1, 5        1, 10
    
    Note that this implementation has not been verified by the original 
    authors. 
    
    """

    # start the model
    input_main   = Input((Chans, Samples, 1))
    block1       = Conv2D(25, (1, 5), 
                                 input_shape=(Chans, Samples, 1),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(input_main)
    block1       = Conv2D(25, (Chans, 1),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(block1)
    block1       = BatchNormalization(epsilon=1e-05, momentum=0.1)(block1)
    block1       = Activation('elu')(block1)
    block1       = MaxPooling2D(pool_size=(1, 2), strides=(1, 2))(block1)
    block1       = Dropout(dropoutRate)(block1)
  
    block2       = Conv2D(50, (1, 5),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(block1)
    block2       = BatchNormalization(epsilon=1e-05, momentum=0.1)(block2)
    block2       = Activation('elu')(block2)
    block2       = MaxPooling2D(pool_size=(1, 2), strides=(1, 2))(block2)
    block2       = Dropout(dropoutRate)(block2)
    
    block3       = Conv2D(100, (1, 5),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(block2)
    block3       = BatchNormalization(epsilon=1e-05, momentum=0.1)(block3)
    block3       = Activation('elu')(block3)
    block3       = MaxPooling2D(pool_size=(1, 2), strides=(1, 2))(block3)
    block3       = Dropout(dropoutRate)(block3)
    
    block4       = Conv2D(200, (1, 5),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(block3)
    block4       = BatchNormalization(epsilon=1e-05, momentum=0.1)(block4)
    block4       = Activation('elu')(block4)
    block4       = MaxPooling2D(pool_size=(1, 2), strides=(1, 2))(block4)
    block4       = Dropout(dropoutRate)(block4)
    
    flatten      = Flatten()(block4)
    
    dense        = Dense(nb_classes, kernel_constraint = max_norm(0.5))(flatten)
    softmax      = Activation('softmax')(dense)
    
    return Model(inputs=input_main, outputs=softmax)


# need these for ShallowConvNet
def square(x):
    return K.square(x)

def log(x):
    return K.log(K.clip(x, min_value = 1e-7, max_value = 10000))   


def ShallowConvNet(nb_classes, Chans = 64, Samples = 128, dropoutRate = 0.5):
    """ Keras implementation of the Shallow Convolutional Network as described
    in Schirrmeister et. al. (2017), Human Brain Mapping.
    
    Assumes the input is a 2-second EEG signal sampled at 128Hz. Note that in 
    the original paper, they do temporal convolutions of length 25 for EEG
    data sampled at 250Hz. We instead use length 13 since the sampling rate is 
    roughly half of the 250Hz which the paper used. The pool_size and stride
    in later layers is also approximately half of what is used in the paper.
    
    Note that we use the max_norm constraint on all convolutional layers, as 
    well as the classification layer. We also change the defaults for the
    BatchNormalization layer. We used this based on a personal communication 
    with the original authors.
    
                     ours        original paper
    pool_size        1, 35       1, 75
    strides          1, 7        1, 15
    conv filters     1, 13       1, 25    
    
    Note that this implementation has not been verified by the original 
    authors. We do note that this implementation reproduces the results in the
    original paper with minor deviations. 
    """

    # start the model
    input_main   = Input((Chans, Samples, 1))
    block1       = Conv2D(40, (1, 13), 
                                 input_shape=(Chans, Samples, 1),
                                 kernel_constraint = max_norm(2., axis=(0,1,2)))(input_main)
    block1       = Conv2D(40, (Chans, 1), use_bias=False, 
                          kernel_constraint = max_norm(2., axis=(0,1,2)))(block1)
    block1       = BatchNormalization(epsilon=1e-05, momentum=0.1)(block1)
    block1       = Activation(square)(block1)
    block1       = AveragePooling2D(pool_size=(1, 35), strides=(1, 7))(block1)
    block1       = Activation(log)(block1)
    block1       = Dropout(dropoutRate)(block1)
    flatten      = Flatten()(block1)
    dense        = Dense(nb_classes, kernel_constraint = max_norm(0.5))(flatten)
    softmax      = Activation('softmax')(dense)
    
    return Model(inputs=input_main, outputs=softmax)

def jsBasicVGGNet(nb_classes, width, height, depth):
    input_main   = Input((width, height, depth))

    block1       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(input_main)
    # block1       = Conv2D(filters=32, kernel_size=4, padding='same', activation='relu')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)
    
    block2       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(block1)
    # block2       = Conv2D(filters=16, kernel_size=4, padding='same', activation='relu')(block2)
    block2       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block2)

    # block3       = Conv2D(filters=8, kernel_size=4, padding='same', activation='relu')(block2)
    # block3       = Conv2D(filters=8, kernel_size=4, padding='same', activation='relu')(block3)
    # block3       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block3)

    block3       = Flatten()(block2)
    block3       = Dense(units=128, activation='relu')(block3)
    block3       = Dropout(0.5)(block3)
    softmax      = Dense(units=nb_classes, activation='softmax')(block3)

    return Model(inputs=input_main, outputs=softmax)

def jsBasicRCNN(nb_classes, width, height, depth):
    input_main   = Input((width, height, depth))

    block1       = Conv2D(filters=256, kernel_size=4, padding='same', activation='relu')(input_main)
    # block1       = Conv2D(filters=32, kernel_size=5, padding='same', activation='relu')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)
    
    block2       = Conv2D(filters=128, kernel_size=4, padding='same', activation='relu')(block1)
    # block2       = Conv2D(filters=16, kernel_size=5, padding='same', activation='relu')(block2)
    block2       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block2)

    block3       = Conv2D(filters=64, kernel_size=4, padding='same', activation='relu')(block2)
    # block2       = Conv2D(filters=16, kernel_size=5, padding='same', activation='relu')(block2)
    block3       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block3)

    block4       = Conv2D(filters=32, kernel_size=4, padding='same', activation='relu')(block3)
    # block2       = Conv2D(filters=16, kernel_size=5, padding='same', activation='relu')(block2)
    block4       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block4)

    block5       = Flatten()(block4)
    block5       = Dense(units=16, activation='relu')(block5)
    block5       = Dropout(0.5)(block5)
    softmax      = Dense(units=nb_classes, activation='softmax')(block5)

    return Model(inputs=input_main, outputs=softmax)

def jsMediumVGGNet(nb_classes, width, height, depth):
    input_main   = Input((width, height, depth))

    block1       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(input_main)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)
    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)

    block2       = Flatten()(block1)
    block2       = Dense(units=16, activation='relu')(block2)
    block2       = Dropout(0.5)(block2)
    softmax      = Dense(units=nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)

def jsMediumRCNNNetTanh(nb_classes, width, height, depth):
    input_main   = Input((width, height, depth))

    block1       = Conv2D(filters=128, kernel_size=3, padding='same', activation='tanh')(input_main)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)
    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='tanh')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)

    block2       = Flatten()(block1)
    block2       = Dense(units=16, activation='tanh')(block2)
    block2       = Dropout(0.5)(block2)
    softmax      = Dense(units=nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)

def jsScaleVGGNet(nb_classes, width, height, depth, CF1, UB2):
    input_main   = Input((width, height, depth))

    block1       = Conv2D(filters=CF1, kernel_size=3, padding='same', activation='relu')(input_main)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)

    block2       = Flatten()(block1)
    block2       = Dense(units=UB2, activation='relu')(block2)
    block2       = Dropout(0.5)(block2)
    softmax      = Dense(units=nb_classes, activation='softmax')(block2)

    return Model(inputs=input_main, outputs=softmax)

def JSModel_Conv1D(nb_classes, chans=19, samples=256):
    
    # input_main = Input((c,timesteps))
    # block1 = Permute((2, 1))(input_main)

    input_main          = Input((chans, samples))
    input_perm          = Permute((2, 1))(input_main)

    conv_layer          = Conv1D(
                            filters = chans*4,
                            kernel_size=16)(input_perm)
    conv_layer          = Conv1D(
                            filters = chans*2,
                            kernel_size=16)(input_perm)
    conv_layer          = MaxPooling1D()(conv_layer)
    conv_layer          = BatchNormalization()(conv_layer)

    flat_layer          = Flatten()(conv_layer)
    flat_layer          = Dense(units=64, activation='relu')(flat_layer)
    flat_layer          = Dropout(0.4)(flat_layer)
    

    if nb_classes == 2:
        dense_out    = Dense(units=1, name = 'outputdense_bin')(flat_layer)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense_out)

    else:
        dense_out    = Dense(units=nb_classes, name = 'outputdense_multiclass')(flat_layer)
        modeloutput  = Activation('softmax', name = 'softmax')(dense_out)

    return Model(inputs=input_main, outputs=modeloutput)
    

def JSModel_Conv2D(nb_classes, height, width, depth, F1=128, F2=256, dense_units = 128, kernel_size=(3,3), dropout=0.5, conv_activation='relu', conv_padding='same', polling_padding='same', dense_activation='relu'):

    input_main   = Input((height, width, depth))

    block1       = Conv2D(filters=F1, kernel_size=kernel_size, padding=conv_padding, activation=conv_activation)(input_main)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding=polling_padding)(block1)

    block1       = Conv2D(filters=F2, kernel_size=(1,64), padding=conv_padding, activation=conv_activation)(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding=polling_padding)(block1)

    block2       = Flatten()(block1)
    block2       = Dense(units=dense_units, activation=dense_activation)(block2)
    block2       = Dropout(dropout)(block2)
    block2       = Dense(units=dense_units, activation=dense_activation)(block2)
    block2       = Dropout(dropout)(block2)
    
    if nb_classes == 2:
        dense_out    = Dense(units=1, name = 'outputdense_bin')(block2)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense_out)

    else:
        dense_out    = Dense(units=nb_classes, name = 'outputdense_multiclass')(block2)
        modeloutput  = Activation('softmax', name = 'softmax')(dense_out)

    return Model(inputs=input_main, outputs=modeloutput)


def JSModel_VGGNet(nb_classes, height, width, depth):
    input_main   = Input((height, width, depth))

    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')(input_main)
    block1       = Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')(block1)
    block1       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block1)

    block2       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(block1)
    block2       = Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')(block2)
    block2       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block2)

    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block2)
    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block3)
    block3       = Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')(block3)
    block3       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block3)

    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block3)
    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block4       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block4       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block4)

    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block4)
    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block5)
    block5       = Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')(block5)
    block5       = MaxPooling2D(pool_size=2, strides=2, padding='same')(block5)

    #Dense Layers
    block6       = Flatten()(block5)
    block6       = Dense(units=4096, activation='relu')(block6)
    block6       = Dropout(0.5)(block6)
    block6       = Dense(units=4096, activation='relu')(block6)
    block6       = Dropout(0.5)(block6)

    if nb_classes == 2:
        dense_out    = Dense(units=1, name = 'outputdense_bin')(block6)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense_out)

    else:
        dense_out    = Dense(units=nb_classes, name = 'outputdense_multiclass')(block6)
        modeloutput  = Activation('softmax', name = 'softmax')(dense_out)

    return Model(inputs=input_main, outputs=modeloutput)

def JSModel_EEGNetMultiHead_CNN(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, dropoutRateDense = 0.5, samplingRate = 128, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout', activation='elu', 
             F1_Dense = 128, F2_Dense = 64):
    
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    
    #### ULF SamplingRate/2
    block1_ulf       = Conv2D(F1, (1, samplingRate//2), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_ulf")(input1)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_ulf)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = Activation(activation)(block1_ulf)
    block1_ulf       = AveragePooling2D((1, 4))(block1_ulf)
    block1_ulf_out   = dropoutType(dropoutRate)(block1_ulf)

    block2_ulf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_ulf_out)
    block2_ulf       = BatchNormalization()(block2_ulf)
    block2_ulf       = Activation(activation)(block2_ulf)
    block2_ulf       = AveragePooling2D((1, 8))(block2_ulf)
    block2_ulf       = dropoutType(dropoutRate)(block2_ulf)
        
    flatten_ulf      = Flatten(name = 'flatten_ulf')(block2_ulf)

    #### LF SampligRate/4
    block1_lf       = Conv2D(F1, (1, samplingRate//4), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_lf")(input1)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_lf)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = Activation(activation)(block1_lf)
    block1_lf       = AveragePooling2D((1, 4))(block1_lf)
    block1_lf_out   = dropoutType(dropoutRate)(block1_lf)

    block2_lf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_lf_out)
    block2_lf       = BatchNormalization()(block2_lf)
    block2_lf       = Activation(activation)(block2_lf)
    block2_lf       = AveragePooling2D((1, 8))(block2_lf)
    block2_lf       = dropoutType(dropoutRate)(block2_lf)
        
    flatten_lf      = Flatten(name = 'flatten_lf')(block2_lf)

    #### LF SampligRate/8
    block1_hf       = Conv2D(F1, (1, samplingRate//8), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_hf")(input1)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_hf)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = Activation(activation)(block1_hf)
    block1_hf       = AveragePooling2D((1, 4))(block1_hf)
    block1_hf_out   = dropoutType(dropoutRate)(block1_hf)

    block2_hf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_hf_out)
    block2_hf       = BatchNormalization()(block2_hf)
    block2_hf       = Activation(activation)(block2_hf)
    block2_hf       = AveragePooling2D((1, 8))(block2_hf)
    block2_hf       = dropoutType(dropoutRate)(block2_hf)
        
    flatten_hf      = Flatten(name = 'flatten_hf')(block2_hf)

    #### LF SampligRate/16
    block1_uhf       = Conv2D(F1, (1, samplingRate//16), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_uhf")(input1)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_uhf)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = Activation(activation)(block1_uhf)
    block1_uhf       = AveragePooling2D((1, 4))(block1_uhf)
    block1_uhf_out   = dropoutType(dropoutRate)(block1_uhf)

    block2_uhf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_uhf_out)
    block2_uhf       = BatchNormalization()(block2_uhf)
    block2_uhf       = Activation(activation)(block2_uhf)
    block2_uhf       = AveragePooling2D((1, 8))(block2_uhf)
    block2_uhf       = dropoutType(dropoutRate)(block2_uhf)
        
    flatten_uhf      = Flatten(name = 'flatten_uhf')(block2_uhf)

    ##############
    
    flatten = Concatenate(axis=1)([flatten_ulf, flatten_lf, flatten_hf, flatten_uhf])

    #### Flatten

    # flatten_global  = Flatten(name = 'flatten_global')(concat_global_block)

    # flatten       = Dense(1024)(concat_global_block)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(512)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(256)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(128)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    dense       = Dense(F1_Dense, name='Dense_F1')(flatten)
    dense       = Dropout(dropoutRateDense)(dense)
    dense       = Dense(F2_Dense, name='Dense_F2')(dense)
    dense       = Dropout(dropoutRateDense)(dense)

    if nb_classes == 1:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(dense)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(dense)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)

def JSModel_EEGNetMultiHead(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, dropoutRateDense = 0.5, samplingRate = 128, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout', activation='elu'):
    """ Keras Implementation of EEGNet
    http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta

    Note that this implements the newest version of EEGNet and NOT the earlier
    version (version v1 and v2 on arxiv). We strongly recommend using this
    architecture as it performs much better and has nicer properties than
    our earlier version. For example:
        
        1. Depthwise Convolutions to learn spatial filters within a 
        temporal convolution. The use of the depth_multiplier option maps 
        exactly to the number of spatial filters learned within a temporal
        filter. This matches the setup of algorithms like FBCSP which learn 
        spatial filters within each filter in a filter-bank. This also limits 
        the number of free parameters to fit when compared to a fully-connected
        convolution. 
        
        2. Separable Convolutions to learn how to optimally combine spatial
        filters across temporal bands. Separable Convolutions are Depthwise
        Convolutions followed by (1x1) Pointwise Convolutions. 
        
    
    While the original paper used Dropout, we found that SpatialDropout2D 
    sometimes produced slightly better results for classification of ERP 
    signals. However, SpatialDropout2D significantly reduced performance 
    on the Oscillatory dataset (SMR, BCI-IV Dataset 2A). We recommend using
    the default Dropout in most cases.
        
    Assumes the input signal is sampled at 128Hz. If you want to use this model
    for any other sampling rate you will need to modify the lengths of temporal
    kernels and average pooling size in blocks 1 and 2 as needed (double the 
    kernel lengths for double the sampling rate, etc). Note that we haven't 
    tested the model performance with this rule so this may not work well. 
    
    The model with default parameters gives the EEGNet-8,2 model as discussed
    in the paper. This model should do pretty well in general, although it is
	advised to do some model searching to get optimal performance on your
	particular dataset.

    We set F2 = F1 * D (number of input filters = number of output filters) for
    the SeparableConv2D layer. We haven't extensively tested other values of this
    parameter (say, F2 < F1 * D for compressed learning, and F2 > F1 * D for
    overcomplete). We believe the main parameters to focus on are F1 and D. 

    Inputs:
        
      nb_classes      : int, number of classes to classify
      Chans, Samples  : number of channels and time points in the EEG data
      dropoutRate     : dropout fraction
      kernLength      : length of temporal convolution in first layer. We found
                        that setting this to be half the sampling rate worked
                        well in practice. For the SMR dataset in particular
                        since the data was high-passed at 4Hz we used a kernel
                        length of 32.     
      F1, F2          : number of temporal filters (F1) and number of pointwise
                        filters (F2) to learn. Default: F1 = 8, F2 = F1 * D. 
      D               : number of spatial filters to learn within each temporal
                        convolution. Default: D = 2
      dropoutType     : Either SpatialDropout2D or Dropout, passed as a string.

    """
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    
    #### ULF SamplingRate/2
    block1_ulf       = Conv2D(F1, (1, samplingRate//2), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_ulf")(input1)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_ulf)
    block1_ulf       = BatchNormalization()(block1_ulf)
    block1_ulf       = Activation(activation)(block1_ulf)
    block1_ulf       = AveragePooling2D((1, 4))(block1_ulf)
    block1_ulf_out   = dropoutType(dropoutRate)(block1_ulf)

    block2_ulf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_ulf_out)
    block2_ulf       = BatchNormalization()(block2_ulf)
    block2_ulf       = Activation(activation)(block2_ulf)
    block2_ulf       = AveragePooling2D((1, 8))(block2_ulf)
    block2_ulf       = dropoutType(dropoutRate)(block2_ulf)
        
    flatten_ulf      = Flatten(name = 'flatten_ulf')(block2_ulf)

    #### LF SampligRate/4
    block1_lf       = Conv2D(F1, (1, samplingRate//4), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_lf")(input1)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_lf)
    block1_lf       = BatchNormalization()(block1_lf)
    block1_lf       = Activation(activation)(block1_lf)
    block1_lf       = AveragePooling2D((1, 4))(block1_lf)
    block1_lf_out   = dropoutType(dropoutRate)(block1_lf)

    block2_lf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_lf_out)
    block2_lf       = BatchNormalization()(block2_lf)
    block2_lf       = Activation(activation)(block2_lf)
    block2_lf       = AveragePooling2D((1, 8))(block2_lf)
    block2_lf       = dropoutType(dropoutRate)(block2_lf)
        
    flatten_lf      = Flatten(name = 'flatten_lf')(block2_lf)

    #### LF SampligRate/8
    block1_hf       = Conv2D(F1, (1, samplingRate//8), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_hf")(input1)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_hf)
    block1_hf       = BatchNormalization()(block1_hf)
    block1_hf       = Activation(activation)(block1_hf)
    block1_hf       = AveragePooling2D((1, 4))(block1_hf)
    block1_hf_out   = dropoutType(dropoutRate)(block1_hf)

    block2_hf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_hf_out)
    block2_hf       = BatchNormalization()(block2_hf)
    block2_hf       = Activation(activation)(block2_hf)
    block2_hf       = AveragePooling2D((1, 8))(block2_hf)
    block2_hf       = dropoutType(dropoutRate)(block2_hf)
        
    flatten_hf      = Flatten(name = 'flatten_hf')(block2_hf)

    #### LF SampligRate/16
    block1_uhf       = Conv2D(F1, (1, samplingRate//16), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False, name="CONV_uhf")(input1)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1_uhf)
    block1_uhf       = BatchNormalization()(block1_uhf)
    block1_uhf       = Activation(activation)(block1_uhf)
    block1_uhf       = AveragePooling2D((1, 4))(block1_uhf)
    block1_uhf_out   = dropoutType(dropoutRate)(block1_uhf)

    block2_uhf       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1_uhf_out)
    block2_uhf       = BatchNormalization()(block2_uhf)
    block2_uhf       = Activation(activation)(block2_uhf)
    block2_uhf       = AveragePooling2D((1, 8))(block2_uhf)
    block2_uhf       = dropoutType(dropoutRate)(block2_uhf)
        
    flatten_uhf      = Flatten(name = 'flatten_uhf')(block2_uhf)

    ##############
    
    flatten = Concatenate(axis=1)([flatten_ulf, flatten_lf, flatten_hf, flatten_uhf])

    #### Flatten

    # flatten_global  = Flatten(name = 'flatten_global')(concat_global_block)

    # flatten       = Dense(1024)(concat_global_block)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(512)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(256)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)
    # flatten       = Dense(128)(flatten)
    # flatten       = Dropout(dropoutRateDense)(flatten)

    if nb_classes == 1:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)