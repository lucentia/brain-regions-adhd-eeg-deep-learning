import json
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from os import path
from os import mkdir
from csv import writer
from datetime import datetime
from sys import exit

class JSLog:

    def __init__(self, experiment_name, sub_dir = None):
        
        configuration_path = path.join(
            path.dirname(path.realpath(__file__)),
            '..',
            'configuration.json')

        with open(configuration_path) as f:
            config_data = json.load(f)

        self.experiment_name = experiment_name
        
        if sub_dir:
            self.log_path = config_data['base_log_path'] + sub_dir + "/"
        else:
            self.log_path = config_data['base_log_path']  

        now_str = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
        hash_now_str = str(hash(now_str))

        #self.experiment_log_path = "{0}{1}_{2}_{3}/".format(
        #    self.log_path,
        #    self.experiment_name,
        #    now_str,
        #    hash_now_str.replace('-','_')
        #)

        self.experiment_log_path=path.join(self.log_path, f"{self.experiment_name}_{now_str}_{hash_now_str}")

        self.data_path = config_data['base_data_path']

        
        self.model_log_dir_models = path.join(self.experiment_log_path, 'models')
        self.model_log_dir_checkpoint = path.join(self.experiment_log_path, 'models', 'checkpoint')
        self.model_log_dir_history = path.join(self.experiment_log_path, 'history')
        self.model_log_dir_predict = path.join(self.experiment_log_path, 'predictions')
        self.model_log_dir_results = path.join(self.experiment_log_path, 'results')
        #self.model_log_dir_plots = path.join(self.experiment_log_path, 'plots')

        # self.data_path = config_data['base_data_path']

        
        # self.model_log_dir_models = self.experiment_log_path + 'models/'
        # self.model_log_dir_checkpoint = self.experiment_log_path + 'models/checkpoint/'
        # self.model_log_dir_history = self.experiment_log_path + 'history/'
        # self.model_log_dir_predict = self.experiment_log_path + 'predictions/'
        # self.model_log_dir_results = self.experiment_log_path + 'results/'

        mkdir(self.experiment_log_path)
       
        mkdir(self.model_log_dir_models)
        mkdir(self.model_log_dir_checkpoint)
        mkdir(self.model_log_dir_history)
        mkdir(self.model_log_dir_predict)
        mkdir(self.model_log_dir_results)

        # New in V2.0
        self.output_log_filename = self.experiment_log_path + 'output_log.txt'
        self.output_history_filename = self.experiment_log_path + 'history_log.csv'
        self.output_results_filename = self.experiment_log_path + 'results.csv'

        self.path_results_file_name = self.model_log_dir_results + "results.csv"
        self.path_results_best_file_name = self.model_log_dir_results + "results_best.csv"
        self.path_results_complete_file_name = self.model_log_dir_results + "results_complete.csv"
        self.path_results_complete_grouped_file_name = self.model_log_dir_results + "results_complete_grouped.csv"

        self.path_results_best_channel = self.model_log_dir_results + "results_best_channel.csv"
    
    def w(self, string, show=False):
        self.write_output_log(string, show)

    def write_output_log(self, string, show=False):
        with open(self.output_log_filename, 'a') as f:
            f.write(f"\n [{datetime.now().strftime('%m/%d/%Y %H:%M:%S')}] - {string}")
            if show:
                print(f"[{datetime.now().strftime('%m/%d/%Y %H:%M:%S')}] - {string}")
    
    def get_checkpoint_file_path(self):
        return self.model_log_dir_checkpoint
    
    def init_both_results(self, experiment_parameters):
        reg_params = experiment_parameters + ['iter', 'loss', 'binary_accuracy', 'precision', 'recall', 'tp', 'tn', 'fp', 'fn']
        
        with open(self.path_results_file_name, 'w') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow(reg_params)

        with open(self.path_results_best_file_name, 'w') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow(reg_params)

    def init_results(self, experiment_parameters):

        reg_params = experiment_parameters + ['iter', 'loss', 'binary_accuracy', 'precision', 'recall', 'tp', 'tn', 'fp', 'fn']
        
        with open(self.path_results_file_name, 'w') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow(reg_params)

    def _compute_custom_stats(self, history_step, true_positives, true_negatives, false_positives, false_negatives):

        # Compute Precision
        if (true_positives + false_positives) == 0:
            comp_precision = 0
            self.w(f"Computed Precision divided by zero history_step[{history_step}] tp[{true_positives}] fp[{false_positives}]", show=True)
        else:
            comp_precision = true_positives/(true_positives + false_positives)

        # Compute Recall
        if (true_positives + false_negatives) == 0:
            comp_recall = 0
            self.w(f"Computed Recall divided by zero history_step[{history_step}] tp[{true_positives}] fn[{false_negatives}]", show=True)
        else:
            comp_recall = true_positives/(true_positives + false_negatives)

        # Compute Specificity
        if (true_negatives + false_positives) == 0:
            comp_specificity = 0
            self.w(f"Computed Specificity divided by zero history_step[{history_step}] tn[{true_negatives}] fp[{false_positives}]", show=True)
        else:
            comp_specificity = true_negatives/(true_negatives + false_positives)
        
        # Compute f1-score
        if (comp_precision+comp_recall) == 0:
            comp_f1 = 0
            self.w(f"Computed f1-score divided by zero history_step[{history_step}] p[{comp_precision}] r[{comp_recall}]", show=True)
        else:
            comp_f1 = 2 * ((comp_precision*comp_recall)/(comp_precision+comp_recall))

        return comp_precision, comp_recall, comp_specificity, comp_f1


    def save_history(self, experiment_parameters_name, experiment_parameters_value, history):
        
        history_header = [
            'loss',
            'binary_accuracy',
            'precision',
            'recall',
            'true_positives',
            'true_negatives',
            'false_positives',
            'false_negatives',
            'val_loss',
            'val_binary_accuracy',
            'val_precision',
            'val_recall',
            'val_true_positives',
            'val_true_negatives',
            'val_false_positives',
            'val_false_negatives'
        ]

        computed_header = [
            'comp_precision', 'comp_recall', 'comp_specificity', 'comp_f1',
            'comp_val_precision', 'comp_val_recall', 'comp_val_specificity', 'comp_val_f1']

        if not path.exists(self.output_history_filename):
            with open(self.output_history_filename, 'w') as f:
                w = writer(f)
                history_header = experiment_parameters_name + ['history_step'] + history_header + computed_header
                w.writerow(history_header)

        with open(self.output_history_filename, 'a') as f:
            w = writer(f)
            history_step = 0
            for loss, binary_accuracy, precision, recall, true_positives, true_negatives, false_positives, false_negatives, val_loss, val_binary_accuracy, val_precision, val_recall, val_true_positives, val_true_negatives, val_false_positives, val_false_negatives in zip(
                history.history['loss'], 
                history.history['binary_accuracy'], 
                history.history['precision'],   
                history.history['recall'],
                history.history['true_positives'], 
                history.history['true_negatives'], 
                history.history['false_positives'],   
                history.history['false_negatives'],
                history.history['val_loss'],
                history.history['val_binary_accuracy'], 
                history.history['val_precision'],   
                history.history['val_recall'],
                history.history['val_true_positives'], 
                history.history['val_true_negatives'], 
                history.history['val_false_positives'],   
                history.history['val_false_negatives']):

                comp_precision, comp_recall, comp_specificity, comp_f1 = self._compute_custom_stats(history_step, true_positives, true_negatives, false_positives, false_negatives)

                comp_val_precision, comp_val_recall, comp_val_specificity, comp_val_f1 = self._compute_custom_stats(history_step, val_true_positives, val_true_negatives, val_false_positives, val_false_negatives)

                reg = experiment_parameters_value + [history_step] + [loss, binary_accuracy, precision, recall, true_positives, true_negatives, false_positives, false_negatives, val_loss, val_binary_accuracy, val_precision, val_recall, val_true_positives, val_true_negatives, val_false_positives, val_false_negatives] + [comp_precision, comp_recall, comp_specificity, comp_f1, comp_val_precision, comp_val_recall, comp_val_specificity, comp_val_f1]

                w.writerow(reg)

                history_step += 1

    def save_results(self, experiment_parameters_name, experiment_parameters_values, results):

        results_header = [
            'loss',
            'binary_accuracy',
            'precision',
            'recall',
            'true_positives',
            'true_negatives',
            'false_positives',
            'false_negatives',
        ]

        computed_results_header = [
            'comp_precision', 'comp_recall', 'comp_specificity', 'comp_f1',
        ]

        if not path.exists(self.output_results_filename):
            with open(self.output_results_filename, 'w') as f:
                w = writer(f)
                results_header = experiment_parameters_name + results_header + computed_results_header
                w.writerow(results_header)

        with open(self.output_results_filename, 'a') as f:
            w = writer(f)

            true_positives = results[4]
            true_negatives = results[5]
            false_positives = results[6]
            false_negatives = results[7]

            comp_precision, comp_recall, comp_specificity, comp_f1 = self._compute_custom_stats(0, true_positives, true_negatives, false_positives, false_negatives)

            reg_results = experiment_parameters_values + results + [comp_precision, comp_recall, comp_specificity, comp_f1]
            w.writerow(reg_results)

    
    def JSSaveHistory(self, history, experiment_parameters_values, experiment_parameters, history_file_name):

        with open(self.model_log_dir_history + history_file_name, 'w') as f: 
            w = writer(f)
            history_header = experiment_parameters + ["loss", "acc", "val_loss", "val_acc"]
            w.writerow(history_header)
            for loss, acc, val_loss, val_acc in zip(history.history['loss'], 
                                                    history.history['binary_accuracy'], 
                                                    history.history['val_loss'],   
                                                    history.history['val_binary_accuracy']):
                reg_data = experiment_parameters_values + [loss, acc, val_loss, val_acc]
                w.writerow(reg_data)

    def JSSaveBothResults(self, parameters_values, fold_iter, results, best_results):

        print("SAVE RESULTS: ")
        print(parameters_values)

        print("RESULTS: ")
        reg_results = parameters_values + [fold_iter] + results
        print(reg_results)

        print("BEST RESULTS: ")
        reg_best_results = parameters_values + [fold_iter] + best_results
        print(reg_best_results)

        with open(self.path_results_file_name, 'a') as f:
            log_writer = writer(f)
            log_writer.writerow(reg_results)

        with open(self.path_results_best_file_name, 'a') as f:
            log_writer = writer(f)
            log_writer.writerow(reg_best_results)

    
    def JSSaveResults(self, parameters_values, fold_iter, results):

        print(parameters_values)
        reg_results = parameters_values + [fold_iter] + results
        print(reg_results)
        with open(self.path_results_file_name, 'a') as f:
            log_writer = writer(f)
            log_writer.writerow(reg_results)
    
    def JSGetBestLobe(self):
        data = pd.read_csv(self.path_results_file_name)

        data["Precision_C"] = data["tp"]/(data["tp"] + data["fp"])
        data["Recall_C"] = data["tp"]/(data["tp"] + data["fn"])
        data["Specificity_C"] = data["tn"]/(data["tn"] + data["fp"])
        data["f1_score"] = 2 * ((data["Precision_C"]*data["Recall_C"])/(data["Precision_C"]+data["Recall_C"]))
        data.to_csv(self.path_results_complete_file_name)

        data_grouped = data.groupby(['LOBE']).mean()
        data_grouped.to_csv(self.path_results_complete_grouped_file_name)

        max_value_f1_score = data_grouped['f1_score'].max()
        max_index_f1_score = data_grouped['f1_score'].idxmax()

        best_index_channels = max_index_f1_score.split('_')
        best_channel = best_index_channels[-1]

        with open(self.path_results_best_channel, 'a') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow([best_channel, max_value_f1_score, max_index_f1_score])

        return best_channel, max_value_f1_score, max_index_f1_score

    def JSGetBestChannel(self):
        data = pd.read_csv(self.path_results_file_name)

        data["Precision_C"] = data["tp"]/(data["tp"] + data["fp"])
        data["Recall_C"] = data["tp"]/(data["tp"] + data["fn"])
        data["Specificity_C"] = data["tn"]/(data["tn"] + data["fp"])
        data["f1_score"] = 2 * ((data["Precision_C"]*data["Recall_C"])/(data["Precision_C"]+data["Recall_C"]))
        data.to_csv(self.path_results_complete_file_name)

        data_grouped = data.groupby(['LOBE']).mean()
        data_grouped.to_csv(self.path_results_complete_grouped_file_name)

        max_value_f1_score = data_grouped['f1_score'].max()
        max_index_f1_score = data_grouped['f1_score'].idxmax()

        best_index_channels = max_index_f1_score.split('_')
        best_channel = best_index_channels[-1]

        with open(self.path_results_best_channel, 'a') as log_file_handler:
            log_writer = writer(log_file_handler)
            log_writer.writerow([best_channel, max_value_f1_score, max_index_f1_score])

        return best_channel, max_value_f1_score, max_index_f1_score

    def JSSaveModel(self, model, model_file_name):
        model.save(self.model_log_dir_models + model_file_name)

    def JSNpToJson(self, results):
        resJson = '{ "results":['
        for data in results:
            resJson = resJson + ',' + str(data[0])
        resJson += ']}'
        return resJson


    def JSEvaluateBySubjectBinary(
        self, 
        model, 
        experiment_parameters,
        experiment_parameters_values,
        test, 
        jsData, 
        predictions_file_name,
        lobe=None,
        type_A = 0, 
        type_B = 1):

        with open(self.model_log_dir_predict + predictions_file_name, 'w') as f:
            w = writer(f)
            predictions_header = experiment_parameters + ["Subject", "SubjectType", "Zeros", "Ones", "Results"]
            w.writerow(predictions_header)

            for subject in test:
                subject_types = [type_A,type_B]
                for subject_type in subject_types:
                    mask_subject, subject_data = jsData.JSGetSingleSubjectBinary(subject, subject_type, lobe)
                    results = model.predict(subject_data)

                    num_zeros = results < 0.5
                    num_ones = results >= 0.5
                    predictions_reg_data = experiment_parameters_values + [subject, subject_type, num_zeros.sum(), num_ones.sum(), self.JSNpToJson(results)]
                    w.writerow(predictions_reg_data)
                    print("SUBJECT {} | TYPE {} | ZEROS {} | ONES {}".format(subject, subject_type, num_zeros.sum(), num_ones.sum()))
                    
                    subject_data = None
                    results = None
                    num_zeros = None
                    num_ones = None


    def JSEvaluateBySubject(
        self, 
        model, 
        experiment_parameters,
        experiment_parameters_values,
        test, 
        jsData, 
        predictions_file_name, 
        type_A, 
        type_B):
    
        with open(self.model_log_dir_predict + predictions_file_name, 'w') as f:
            w = writer(f)
            predictions_header = experiment_parameters + ["Subject", "SubjectType", "Zeros", "Ones", "Results"]
            w.writerow(predictions_header)

            for subject in test:
                subject_types = [type_A,type_B]
                for subject_type in subject_types:

                    subject_data = jsData.JSGetSingleSubject(subject, subject_type)
                    results = model.predict(subject_data)

                    num_zeros = results < 0.5
                    num_ones = results >= 0.5
                    predictions_reg_data = experiment_parameters_values + [subject, subject_type, num_zeros.sum(), num_ones.sum(), self.JSNpToJson(results)]
                    w.writerow(predictions_reg_data)
                    print("SUBJECT {} | TYPE {} | ZEROS {} | ONES {}".format(subject, subject_type, num_zeros.sum(), num_ones.sum()))
                    
                    subject_data = None
                    results = None
                    num_zeros = None
                    num_ones = None

    def _plot_eeg(self, eeg_sample, sample_offset, sampling_frequency, title, ch_names, output_dir, window_size, overlapping, fmin, fmax):
    
        vspace=1200
        color='k'
        
        num_channels = eeg_sample.shape[0]
        num_time_steps = eeg_sample.shape[1]

        bases = np.flip(vspace * np.arange(num_channels))
        data = (eeg_sample.T + bases)

        self.w(f"PLOT EEG DATA - type {type(data)} - shape {data.shape}")

        time = (np.arange(num_time_steps) / sampling_frequency) + ((sample_offset-1)*window_size)

        fig_a = plt.figure(figsize=[14, 4.326])
        plt.margins(x=0)
        # Plot EEG versus time
        plt.plot(time, data, color=color, linewidth=0.5)

        # Add gridlines to the plot
        plt.grid()

        # Label the axes
        plt.xlabel('Time (s)')
        plt.ylabel('Channels')
        
        # The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
        # default names for them.
        plt.gca().yaxis.set_ticks(bases)
        plt.gca().yaxis.set_ticklabels(ch_names)
        
        # Put a nice title on top of the plot
        plt.title(title)

        fig_a.set_tight_layout(True)

        fig_a.savefig(path.join(output_dir, f"time_{sample_offset:02d}.png"), dpi=300.)
        #plt.show()
        plt.close(fig_a)
        
    
    def _plot_s_transform(self, s_transform_img, subject_type, index_subject, sample_offset, ch_names, output_dir, window_size, overlapping, fmin, fmax):

        offset = (sample_offset - 1)*window_size
        extent = (offset, offset + window_size, fmin, fmax)
        index_channel = 1
        s_transform_data = np.dsplit(s_transform_img, s_transform_img.shape[2])

        for s_transform in s_transform_data:

            s_transform = np.reshape(s_transform, (s_transform.shape[0], s_transform.shape[1]))
            fig = plt.figure()
            plt.imshow(s_transform, origin='lower', extent=extent)
            plt.axis('tight')
            plt.xlabel('time (s)')
            plt.ylabel('frequency (Hz)')
            plt.title(f"[{subject_type}] - ID [{index_subject}][{sample_offset}] - [{ch_names[index_channel-1]}]")
            
            output_dir_s_transform_img = path.join(output_dir, f"s_transform_time_{sample_offset:02d}")
            if not path.exists(output_dir_s_transform_img):
                mkdir(output_dir_s_transform_img)

            fig.savefig(path.join(output_dir_s_transform_img, f"s_trans_ch_{index_channel:02d}_{ch_names[index_channel-1]}.png"), dpi=300.)
            #plt.show()
            plt.close(fig)
            index_channel+=1

    def save_s_transform_imgs(
        self, stransform_img_dirname, preprocessing_data_type, subject_type, subject_type_id, index_subject,
        eeg_sample, sample_offset, sampling_frequency, title, ch_names,
        s_transform_img, window_size, overlapping, fmin, fmax):
        
        output_dir_data = path.join(self.data_path, "spectral_analysis", stransform_img_dirname)
        if not path.exists(output_dir_data):
            mkdir(output_dir_data)

        output_dir_data_type = path.join(output_dir_data, subject_type)
        if not path.exists(output_dir_data_type):
            mkdir(output_dir_data_type)

        output_dir_data_type_subject = path.join(output_dir_data_type, f"{index_subject:02d}")
        if not path.exists(output_dir_data_type_subject):
            mkdir(output_dir_data_type_subject)

        self._plot_eeg(
            eeg_sample, 
            sample_offset, 
            sampling_frequency, 
            f"{subject_type} - {index_subject} - {sample_offset}", 
            ch_names, 
            output_dir_data_type_subject,
            window_size, 
            overlapping,
            fmin, 
            fmax)

        self._plot_s_transform(
                s_transform_img, 
                subject_type, 
                index_subject, 
                sample_offset, 
                ch_names, 
                output_dir_data_type_subject,
                window_size, 
                overlapping,
                fmin, 
                fmax)

    def save_data_statistics(self, data, header):
        
        #Stores statistics for readed data
        with open(path.join(self.experiment_log_path, 'data_statistics.csv'), 'w') as f:
      
            write = writer(f)
            
            write.writerow(header)
            for clave, valor in data.items():
                write.writerow(valor)

    def create_s_transform_statistics(self, s_transform_statistics_header):
        
        with open(path.join(self.experiment_log_path, 's_transform_data_statistics.csv'), 'w') as f:
      
            write = writer(f)
            write.writerow(s_transform_statistics_header)

    def append_s_transform_statistics_row(self, s_transform_statistics_row):
        
        with open(path.join(self.experiment_log_path, 's_transform_data_statistics.csv'), 'a') as f:
      
            write = writer(f)
            write.writerow(s_transform_statistics_row)

    def save_model_labels(self, labels):
        
        np.savetxt(path.join(self.experiment_log_path, 's_transform_model_labels.csv'), labels, delimiter=',')

        

        