# Author: Javier Sanchis <javier.sanchis@ua.es>

# Copyright 2022 Javier Sanchis

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import sys
import json
import h5py
import numpy as np
import pandas as pd

from gc import collect

from numpy import dtype, isin
from numpy import zeros
from numpy import ones
from numpy import concatenate
from numpy import random
from numpy import delete
from numpy import where
from numpy import load
from itertools import combinations


from sklearn.utils import shuffle


from os import path
from os import listdir

# from tqdm import tqdm

import scipy.io as spio

import math as m

import matplotlib.pyplot as plt

import JSLib.JSPreprocessing as JSprep



# dir_includes = self.dir_path + '/lib/'
# dir_data = self.dir_path + '/data/'
# input_data = 'dataScaled'
# input_data_path = self.dir_data + input_datatype + '.npy'
# input_labels_path = dir_data + 'labels.npy'

class JSData:

    def __init__(self, data_name):

        configuration_path = path.join(
            path.dirname(path.realpath(__file__)),
            '..',
            'configuration.json')
        

        with open(configuration_path) as f:
            config_data = json.load(f)

        self.dir_path = path.dirname(path.realpath(__file__))
        self.dir_data = config_data['base_data_path']
        print("Loading Data Format: ", data_name)
        self.num_channels = -1
        self.length_channels = -1
        self._init_locations()
        self._init_available_channels()
        self._init_backward_SFS()
        
        
        if data_name == 'raw':
            self.load_raw_data()

        elif data_name == 'raw_stats':
             self.load_raw_stats_data()
    
    def setBackwardSFS(self, best_lobe):

        self.channel_combinations = {}

        if self.backward_SFS_iteration == 0:
            # all channels
            key = '_'.join(self.available_channels)
            self.channel_combinations[key] = [self.channels[chan] for chan in self.available_channels]

        else:
            if self.backward_SFS_iteration > 1:
                print("\nITERATION [{}]\n".format(self.backward_SFS_iteration))
                
                # Find Worst Channel
                best_lobe_list = best_lobe.split('_')
                worst_channel = self._js_diff(self.available_channels, best_lobe_list)
                worst_channel = worst_channel[0]

                print("AVAILABLE CHANNELS [{}]\n".format(self.available_channels))
                print("BEST LOBE: \n\t LIST [{}]\n\t KEY [{}]\n\t WORST CHANNEL [{}]\n\t LEN [{}]".format(
                    best_lobe_list, best_lobe, worst_channel, len(best_lobe_list)))

                # Remove worst channel from available and append to worst channel list
                i_tmp = 1
                for c_tmp in self.available_channels:
                    print("[{}] - CH [{}]({}) - [{}][{}]".format(
                        i_tmp, 
                        c_tmp, type(c_tmp), 
                        worst_channel, type(worst_channel)))

                self.available_channels.remove(worst_channel)
                self.channel_worst_combination.append(worst_channel)

            channel_combinations_tmp = combinations(self.available_channels, len(self.available_channels) - 1)

            for channel_combination in channel_combinations_tmp:
                key = '_'.join(channel_combination)
                self.channel_combinations[key] = [self.channels[chan] for chan in channel_combination]

        self.backward_SFS_iteration += 1

    def setChannelsCombination(self, best_new_channel):
        # Best Combination are channel names in list
        # 

        self.channel_combinations = {}
        
        if best_new_channel:
            self.available_channels.remove(best_new_channel)
            self.channel_best_combination.append(best_new_channel)

            for channel_name in self.available_channels:
                key = "_".join(self.channel_best_combination) + "_" + channel_name
                tmp_chan_list = [self.channels[chan] for chan in self.channel_best_combination]
                tmp_chan_list.append(self.channels[channel_name])
                self.channel_combinations[key] = tmp_chan_list

        else:
            for channel_name in self.available_channels:
                self.channel_combinations[channel_name] = [self.channels[channel_name]]

    def existsAvailableChannels(self):
        return len(self.available_channels) > 0

    def _js_diff(self, list_a, list_b):
        # Compares two lists and returns the differing elements.
        # list_a bigger than list_b

        s = set(list_b)
        differences = [x for x in list_a if x not in s]
        return differences


    def _init_backward_SFS(self):
        self.backward_SFS_iteration = 0
        self.channel_worst_combination = []

    def _init_available_channels(self):
        self.available_channels = [key for key in self.channels.keys()]
        # Dict 
        self.channel_combinations = {}
        
        self.channel_best_combination = []

    def _init_locations(self):
        
        self.channels = {
            "Fp1":0,
            "Fp2":1,
            "F3":2,
            "F4":3,
            "C3":4,
            "C4":5,
            "P3":6,
            "P4":7,
            "O1":8,
            "O2":9,
            "F7":10,
            "F8":11,
            "T7":12,
            "T8":13,
            "P7":14,
            "P8":15,
            "Fz":16,
            "Cz":17,
            "Pz":18
        }
        self.brain_locations = {
            # Frontal Lobe
            "FL":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"]
                ],
            
            # Frontal Lobe Only
            "FL_":[
                self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"]
                ],
            
            # Frontal Lobe Right
            "FL_R":[
                self.channels["F4"],
                self.channels["F8"]
                ],

            # Frontal Lobe Left
            "FL_L":[
                self.channels["F7"],
                self.channels["F3"]
                ]
        }
        self.brain_locations_ = {
            # Right Hemisphere
            "RH":[
                self.channels["Fp2"], self.channels["F4"], self.channels["C4"], 
                self.channels["P4"], self.channels["O2"], self.channels["F8"], 
                self.channels["T8"], self.channels["P8"]
                ],
            
            # Left Hemisphere
            "LH":[
                self.channels["Fp1"], self.channels["F3"], self.channels["C3"],
                self.channels["P3"], self.channels["O1"], self.channels["F7"],
                self.channels["T7"], self.channels["P7"]
                ],

            # Frontal Lobe
            "FL":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"]
                ],

            # Parietal Lobe
            "PL":[
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"]
                ],

            # Occipital Lobe
            "OL":[
                self.channels["O1"], self.channels["O2"]
                ],

            # Temporal Lobes
            "TL":[
                self.channels["T7"], self.channels["T8"]
                ],

            # Central
            "C":[
                self.channels["C3"], self.channels["Cz"], self.channels["C4"]
            ],

            # Frontal, Temporal, Central
            "FTC":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["T7"], self.channels["T8"],

                self.channels["C3"], self.channels["Cz"], self.channels["C4"]
            ],

            # Frontal, Temporal, Parietal
            "FTP":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],

                self.channels["T7"], self.channels["T8"], 

                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"]
            ],

            # Frontal, Temporal, Occipital
            "FTO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["T7"], self.channels["T8"], 

                self.channels["O1"], self.channels["O2"]
            ],

            # Frontal, Central, Parietal
            "FCP":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"]
            ],

            # Frontal, Central, Occipital
            "FCO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Frontal, Parietal, Occipital
            "FPO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"],

                self.channels["O1"], self.channels["O2"]
            ],

            # Temporal, Central, Parietal
            "TCP":[
                self.channels["T7"], self.channels["T8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"]
            ],

            # Temporal, Central, Occipital
            "TCO":[
                self.channels["T7"], self.channels["T8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Temporal, Parietal, Occipital
            "TPO":[
                self.channels["T7"], self.channels["T8"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"],
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Central, Parietal, Occipital
            "CPO":[
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],

                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"],
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Frontal, Temporal, Parietal, Occipital
            "FTPO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["T7"], self.channels["T8"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"],
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Frontal, Central, Parietal, Occipital
            "FCPO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"], 
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"], 
                
                self.channels["O1"], self.channels["O2"]
            ],

            # Frontal, Temporal, Central, Occipital
            "FTCO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["T7"], self.channels["T8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["O1"], self.channels["O2"]

            ],

            # Frontal, Temporal, Central, Parietal
            "FTCP":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],
                
                self.channels["T7"], self.channels["T8"],
                
                self.channels["C3"], self.channels["Cz"], self.channels["C4"],
                
                self.channels["P7"], self.channels["P3"], self.channels["Pz"],
                self.channels["P4"], self.channels["P8"]

            ],

            # Temporal, Central, Parietal, Occipital
            "TCPO":[
                self.channels["T7"], self.channels["T8"],

                self.channels["C3"], self.channels["Cz"], self.channels["C4"],

                self.channels["P7"], self.channels["P3"], self.channels["Pz"], 
                self.channels["P4"], self.channels["P8"],

                self.channels["O1"], self.channels["O2"]


            ],

            # Frontal, Temporal, Central, Parietal, Occipital (ALL)
            "FTCPO":[
                self.channels["Fp1"], self.channels["Fp2"], self.channels["F7"],
                self.channels["F3"], self.channels["Fz"], self.channels["F4"],
                self.channels["F8"],

                self.channels["T7"], self.channels["T8"],

                self.channels["C3"], self.channels["Cz"], self.channels["C4"],

                self.channels["P7"], self.channels["P3"], self.channels["Pz"], 
                self.channels["P4"], self.channels["P8"],

                self.channels["O1"], self.channels["O2"]

            ],


            "Fp1":[
                self.channels["Fp1"]
            ],
            "Fp2":[
                self.channels["Fp2"]
            ],
            "F3":[
                self.channels["F3"]
            ],
            "F4":[
                self.channels["F4"]
            ],
            "C3":[
                self.channels["C3"]
            ],
            "C4":[
                self.channels["C4"]
            ],
            "P3":[
                self.channels["P3"]
            ],
            "P4":[
                self.channels["P4"]
            ],
            "O1":[
                self.channels["O1"]
            ],
            "O2":[
                self.channels["O2"]
            ],
            "F7":[
                self.channels["F7"]
            ],
            "F8":[
                self.channels["F8"]
            ],
            "T7":[
                self.channels["T7"]
            ],
            "T8":[
                self.channels["T8"]
            ],
            "P7":[
                self.channels["P7"]
            ],
            "P8":[
                self.channels["P8"]
            ],
            "Fz":[
                self.channels["Fz"]
            ],
            "Cz":[
                self.channels["Cz"]
            ],
            "Pz":[
                self.channels["Pz"]
            ],
        }

    def _load_dir(self, dir):
        """ Loads preprocessed data from https://ieee-dataport.org/open-access/eeg-data-adhd-control-children.

        Parameters
        ----------
        
        Returns
        -------
        samples : nparray, shape (n_timesteps, n_channels)
           nparray with the EEG data from dir
        subjects : list, shape (num_subjects)
            List of integers indicating id_subject
        """

        files = listdir(dir)
        files.sort()

        # Using the same subjects as in the study
        files = files [0:60]
        

        samples = []
        subjects = []

        print("Reading data from [{}]".format(dir))
        #with tqdm(total=len(files), position=0, leave=True, desc="Reading files") as pbar:
        for file in files:
            
            data = pd.read_csv(path.join(dir, file), sep="\t")
            np_array = data.drop(columns=['Time']).to_numpy()
            samples.append(np_array)
            subjects.append(int(path.splitext(file)[0]))

        #        pbar.update()
        #    pbar.close()

        return np.asarray(samples, dtype=object), np.asarray(subjects, dtype=np.int32)

    def load_data(self):
        """ Loads preprocessed data from https://ieee-dataport.org/open-access/eeg-data-adhd-control-children.

        Parameters
        ----------
        
        Returns
        -------
        control_samples : list, shape (num_subjects, [n_timesteps], n_channels)
            Ragged list of nparrays with the EEG data from control subjects
        control_subjects : list, shape (num_subjects)
            List of integers indicating id_subject and type
        adhd_samples : list, shape (num_subjects, [n_timesteps], n_channels)
            Ragged list of nparrays with the EEG data from ADHD subjects
        adhd_subjects : list, shape (num_subjects)
            List of integers indicating id_subject and type
        """

        dir_path_control = path.join(self.dir_data, "ds", "CONTROL", "csv")
        dir_path_adhd = path.join(self.dir_data, "ds", "ADHD", "csv")

        control_samples, control_subjects = self._load_dir(dir_path_control)
        adhd_samples, adhd_subjects = self._load_dir(dir_path_adhd)

        return control_samples, control_subjects, adhd_samples, adhd_subjects

    def load_raw_stats_data(self):
        dir_path_control = path.join(self.dir_data, "ds", "CONTROL", "csv")
        dir_path_adhd = path.join(self.dir_data, "ds", "ADHD", "csv")

        sampling_frequency = 128

        self.num_subjects = 60
        self.sample_frequency = sampling_frequency  
        self.sample_rate = sampling_frequency 

        control_num_samples = []
        adhd_num_samples = []

        for subject in range(1,61):
            subject_code = "{}".format(subject).zfill(2)

            subject_file_control = dir_path_control + subject_code + ".csv"
            data_control = pd.read_csv(subject_file_control, sep="\t")
            print(subject_file_control)
            print(data_control.shape)
            control_num_samples.append(data_control.shape[0])

            subject_file_adhd = dir_path_adhd + subject_code + ".csv"
            data_adhd = pd.read_csv(subject_file_adhd, sep="\t")
            print(subject_file_adhd)
            print(data_adhd.shape)
            adhd_num_samples.append(data_adhd.shape[0])

        df_samples_length = pd.DataFrame()

        df_samples_length['controls'] = control_num_samples
        df_samples_length['adhd'] = adhd_num_samples

        print(df_samples_length.describe())
        print(df_samples_length.describe()/self.sample_rate)
        print(df_samples_length.sum())
        print(df_samples_length.sum()/self.sample_rate)

    def load_preprocessed_data(self, window_size, overlapping):
        dir_path_control = path.join(self.dir_data, "ds", "CONTROL", "csv")
        dir_path_adhd = path.join(self.dir_data, "ds", "ADHD", "csv")

    def load_raw_data(self):
        """ Loads preprocessed data from https://ieee-dataport.org/open-access/eeg-data-adhd-control-children. Loads 2-second frames with 50% overlapping. Deprecated

        Parameters
        ----------
        
        Returns
        -------
        
        """

        dir_path_control = path.join(self.dir_data, "ds", "CONTROL", "csv")
        dir_path_adhd = path.join(self.dir_data, "ds", "ADHD", "csv")

        sampling_frequency = 128
        window_size = int(2*sampling_frequency)
        overlapping = int(0.5*sampling_frequency)   

        subject_list = []
        sample_list = []
        label_list = []
        self.num_subjects = 60
        self.sample_frequency = sampling_frequency  
        self.sample_rate = sampling_frequency 
        for subject in range(1,61):
            subject_code = "{}".format(subject).zfill(2)
            
            subject_file_control = path.join(dir_path_control, subject_code + ".csv")
            data_control = pd.read_csv(subject_file_control, sep="\t")

            np_array_control = data_control.drop(columns=['Time']).to_numpy()
            print("Subject Control: {} [{}]".format(subject, np_array_control.shape))

            start = 0
            np_array_control = JSprep.JSScaleEEGSample(np_array_control)

            np_array_control = np_array_control.transpose()

            print("Subject Control: {} [{}]".format(subject, np_array_control.shape))
            
            while start + window_size < np_array_control.shape[1]:
                #print("Start: {} End:{}".format(start, start+window_size))
                sample = np_array_control[:,start:start + window_size]
                sample_list.append(sample)
                label_list.append((subject,0))

                start += overlapping

            subject_file_adhd = path.join(dir_path_adhd, subject_code + ".csv")
            data_adhd = pd.read_csv(subject_file_adhd, sep="\t")

            np_array_adhd = data_adhd.drop(columns=['Time']).to_numpy()

            np_array_adhd = JSprep.JSScaleEEGSample(np_array_adhd)
            np_array_adhd = np_array_adhd.transpose()

            start = 0
            
            print("Subject Adhd: {} [{}]".format(subject, np_array_adhd.shape))
            while start + window_size < np_array_adhd.shape[1]:
                #print("Start: {} End:{}".format(start, start+window_size))
                sample = np_array_adhd[:,start:start + window_size]
                sample_list.append(sample)
                label_list.append((subject,1))

                start += overlapping
            
            #print(len(sample_list))
            #print(len(label_list))
            
        self.np_data = np.array(sample_list)
        self.np_labels = np.array(label_list)

        print(self.np_data.shape)
        print(self.np_labels.shape)


    def load_mat_files(self, data_name):
        num_files = 7
        for file_num in range(1,num_files+1):
            file_name = "{}{}{}.mat".format(self.dir_data, data_name, file_num)
            print("Reading file {}".format(file_name))
            data_key = 'd{}'.format(file_num)
            data_read = np.transpose(h5py.File(file_name, 'r')[data_key])
            if file_num == 1:
                self.np_data = data_read
            else:
                self.np_data = np.vstack((self.np_data, data_read))

    def load_files(self, data_name):
        num_files = 7
        
        for file_num in range(1,num_files+1):
            file_name = "{}{}_0{}.npy".format(self.dir_data, data_name, file_num)
            print("Reading file {}".format(file_name))
            data = np.load(file_name)
            data_rs = np.reshape(data, (data.shape[0], data.shape[2]))

            print(data_rs[10,10])
            print(data[10][0][10])

            print(data_rs[12,12])
            print(data[12][0][12])

            print(data.shape)
            if file_num == 1:
                self.np_data = data_rs
            else:
                self.np_data = np.vstack((self.np_data, data_rs))

        print(self.np_data.shape)

    def load_d_spec(self, data_name):
        num_files = 7

        for file_num in range(1,num_files+1):
            file_name = "{}{}_0{}.mat".format(self.dir_data, data_name, file_num)
            print("Reading file {}".format(file_name))
            data_key = 'X'
            if data_name == 'd_spec_r':
                data_key = 'X_R'
            
            if file_num == 1:
                self.np_data = np.transpose(h5py.File(file_name, 'r')[data_key])
            else:
                self.np_data = np.concatenate((self.np_data, np.transpose(h5py.File(file_name, 'r')[data_key])))

        if data_name == '':
            print('Loading Data File:', self.dir_data + 'y_log.mat')
            self.np_labels = np.transpose(h5py.File(self.dir_data + 'y_log.mat', 'r')['Y'])
        elif data_name == 'd_spec_r':
            print('Loading Data File:', self.dir_data + 'y_log.mat')
            self.np_labels = np.transpose(h5py.File(self.dir_data + 'y_d_spec_r.mat', 'r')['Y'])
        else:
            print('Loading Data File:', self.dir_data + 'labels.npy')
            self.np_labels = load(self.dir_data + 'labels.npy')

            print(self.np_data.shape)

    def JSCart2sph(self, x, y, z):
        """
        Transform Cartesian coordinates to spherical
        :param x: X coordinate
        :param y: Y coordinate
        :param z: Z coordinate
        :return: radius, elevation, azimuth
        """
        x2_y2 = x**2 + y**2
        r = m.sqrt(x2_y2 + z**2)                    # rtant^(-1)(y/x)
        elev = m.atan2(z, m.sqrt(x2_y2))            # Elevation
        az = m.atan2(y, x)                          # Azimuth
        return r, elev, az


    def JSPol2cart(self, theta, rho):
        """
        Transform polar coordinates to Cartesian 
        :param theta: angle value
        :param rho: radius value
        :return: X, Y
        """
        return rho * m.cos(theta), rho * m.sin(theta)

    def JSAzim_proj(self, pos):
        """
        Computes the Azimuthal Equidistant Projection of input point in 3D Cartesian Coordinates.
        Imagine a plane being placed against (tangent to) a globe. If
        a light source inside the globe projects the graticule onto
        the plane the result would be a planar, or azimuthal, map
        projection.

        :param pos: position in 3D Cartesian coordinates    [x, y, z]
        :return: projected coordinates using Azimuthal Equidistant Projection
        """
        [r, elev, az] = self.JSCart2sph(pos[0], pos[1], pos[2])
        return self.JSPol2cart(az, m.pi / 2 - elev)

    def JSGetChannels(self):
        spioData = spio.loadmat(self.dir_data + '/chan.mat')

        # print(spioData['chan']['labels'][0][0][0])
        channelNames = []
        for channelData in spioData['chan']['labels'][0]:
            # print(channelData[0])
            if channelData[0] not in ['P9', 'P10', 'P11', 'P12']:
                channelNames.append(channelData[0])
        return channelNames


    def JSGetSingleSubjectBinary(self, id_subject, type_subject, lobe=None):

        eeg_data = self.np_data
        if(lobe):
            locations = self.brain_locations[lobe]
            eeg_data = self.np_data[: , locations, :]

        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,1], type_subject)
        return mask_subject, eeg_data[mask_subject]

    def JSGetSingleSubjectRaw(self, id_subject, type_subject):
        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,type_subject], [1])
        return self.np_data[mask_subject]

    def JSGetSingleSubject(self, id_subject, type_subject):
        mask_subject = isin(self.np_labels[:,0], id_subject) & isin(self.np_labels[:,type_subject], [1])
        ret_data = self.np_data[mask_subject]
        return ret_data.reshape(ret_data.shape[0], self.num_channels * self.length_channels)

    def JSGetTrainTest(self, train_subjects, test_subjects, int_seed, flat = False, reshape = True, num_channels = 56, channel_length = 385, type_A = 1, type_B = 2, categorized = False, tangent = False):
    ##### TRAIN SUBJECTS

        x_val_subjects = random.choice(train_subjects, 3, False)

        partial_x_train_subjects = delete(train_subjects, where( (train_subjects==x_val_subjects[0]) | (train_subjects==x_val_subjects[1]) | (train_subjects==x_val_subjects[2]) ))


        # Search for different types in label and get the indexes
        partial_x_train_mask_type_A = isin(self.np_labels[:,0], partial_x_train_subjects) & isin(self.np_labels[:,type_A], [1])
        x_val_mask_type_A = isin(self.np_labels[:,0], x_val_subjects) & isin(self.np_labels[:,type_A], [1])

        partial_x_train_mask_type_B = isin(self.np_labels[:,0], partial_x_train_subjects) & isin(self.np_labels[:,type_B], [1])
        x_val_mask_type_B = isin(self.np_labels[:,0], x_val_subjects) & isin(self.np_labels[:,type_B], [1])

        # Get train data from indexes
        partial_x_train_type_A = self.np_data[partial_x_train_mask_type_A]
        x_val_type_A = self.np_data[x_val_mask_type_A]

        partial_x_train_type_B = self.np_data[partial_x_train_mask_type_B]
        x_val_type_B = self.np_data[x_val_mask_type_B]

        partial_y_train_type_A = zeros(partial_x_train_type_A.shape[0])
        y_val_type_A = zeros(x_val_type_A.shape[0])

        partial_y_train_type_B = ones(partial_x_train_type_B.shape[0])
        y_val_type_B = ones(x_val_type_B.shape[0])

        if tangent:
            partial_y_train_type_A = partial_y_train_type_A - 1
            y_val_type_A = y_val_type_A - 1

        ##### TEST SUBJECTS

        # Get test indexes
        x_test_mask_subject_type_A = isin(self.np_labels[:,0], test_subjects) & isin(self.np_labels[:,type_A], [1])
        x_test_mask_subject_type_B = isin(self.np_labels[:,0], test_subjects) & isin(self.np_labels[:,type_B], [1])

        # Get test data from test indexes
        x_test_type_A = self.np_data[x_test_mask_subject_type_A]
        x_test_type_B = self.np_data[x_test_mask_subject_type_B]
        
        y_test_type_A = zeros(x_test_type_A.shape[0])
        y_test_type_B = ones(x_test_type_B.shape[0])

        if tangent:
            y_test_type_A = y_test_type_A - 1

        ##### CONCATENATE AND SHUFFLE

        partial_x_train, partial_y_train = shuffle(concatenate([partial_x_train_type_A, partial_x_train_type_B]), 
                                    concatenate([partial_y_train_type_A, partial_y_train_type_B]),
                                    random_state=int_seed)

        x_val, y_val = shuffle(concatenate([x_val_type_A, x_val_type_B]), 
                                    concatenate([y_val_type_A, y_val_type_B]),
                                    random_state=int_seed)

        x_test, y_test = shuffle(concatenate([x_test_type_A, x_test_type_B]), 
                                concatenate([y_test_type_A, y_test_type_B]), 
                                random_state=int_seed)

        if reshape:
            partial_x_train = partial_x_train.reshape(partial_x_train.shape[0], num_channels * channel_length)
            x_val = x_val.reshape(x_val.shape[0], num_channels * channel_length)

            x_test = x_test.reshape(x_test.shape[0], num_channels * channel_length)
        else:
            partial_x_train = partial_x_train.reshape(partial_x_train.shape[0], partial_x_train.shape[1], partial_x_train.shape[2],1)
            x_val = x_val.reshape(x_val.shape[0], x_val.shape[1], x_val.shape[2],1)

            x_test = x_test.reshape(x_test.shape[0], x_test.shape[1], x_test.shape[2], 1)
        # x_val_subjects = None
        # partial_x_train_subjects = None

        

        if categorized:
            partial_y_train_cat = zeros((partial_y_train.shape[0],2))
            cont = 0
            for i in partial_y_train:
                partial_y_train_cat[cont][int(i)] = 1
                cont = cont + 1 

            y_val_cat = zeros((y_val.shape[0],2))
            cont = 0
            for i in y_val:
                y_val_cat[cont][int(i)] = 1
                cont = cont + 1 

            y_test_cat = zeros((y_test.shape[0],2))
            cont = 0
            for i in y_test:
                y_test_cat[cont][int(i)] = 1
                cont = cont + 1 

            partial_y_train = partial_y_train_cat
            y_val = y_val_cat
            y_test = y_test_cat

        partial_x_train_mask_type_A = None
        x_val_mask_type_A = None

        partial_x_train_mask_type_B = None
        x_val_mask_type_B = None

        partial_x_train_type_A = None
        partial_x_train_type_B = None

        partial_y_train_type_A = None
        partial_y_train_type_B = None

        x_val_type_A = None
        x_val_type_B = None

        y_val_type_A = None
        y_val_type_B = None

        x_test_type_A = None
        x_test_type_B = None

        y_test_type_A = None
        y_test_type_B = None

        partial_y_train_cat = None
        y_val_cat = None
        y_test_cat = None

        data_collected = collect()
        
        return partial_x_train, x_val, partial_y_train, y_val, x_test, y_test, partial_x_train_subjects, x_val_subjects, data_collected

    def JSGetTrainTest_Categorical(
        self,
        train_subjects,
        test_subjects,
        num_validation_subjects,
        int_seed,
        lobe=None,
        lobe_origin=None):

        eeg_data = self.np_data

        if(lobe):
            if (lobe_origin == "AUTO" ):
                locations = self.channel_combinations[lobe]
            else:
                locations = self.brain_locations[lobe]

            eeg_data = self.np_data[: , locations, :]

        x_val_subjects = random.choice(train_subjects, num_validation_subjects, False)
        partial_x_train_subjects = train_subjects
        
        for val_subject in x_val_subjects:
            partial_x_train_subjects = delete(partial_x_train_subjects, where(partial_x_train_subjects==val_subject))

        partial_x_train_mask = isin(self.np_labels[:,0], partial_x_train_subjects)
        x_val_mask = isin(self.np_labels[:,0], x_val_subjects)

        partial_x_train = eeg_data[partial_x_train_mask]
        x_val = eeg_data[x_val_mask]

        partial_y_train = self.np_labels[partial_x_train_mask, 1:]
        y_val = self.np_labels[x_val_mask, 1:]

        x_test_mask = isin(self.np_labels[:,0], test_subjects)

        x_test = eeg_data[x_test_mask]
        y_test = self.np_labels[x_test_mask, 1:]

        partial_x_train, partial_y_train = shuffle(
            partial_x_train, 
            partial_y_train,
            random_state=int_seed)

        x_val, y_val = shuffle(
            x_val, 
            y_val,
            random_state=int_seed)
        
        x_test, y_test = shuffle(
            x_test, 
            y_test,
            random_state=int_seed)

        return partial_x_train, partial_y_train, x_val, y_val, x_test, y_test
